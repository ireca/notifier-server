package memory

import (
	"fmt"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"sort"
	"sync"
)

type messageRepository struct {
	mu     *sync.RWMutex
	buffer map[string]models.Message
	bSize  int
}

func NewMessageRepository(buffer map[string]models.Message, bSize int) interfaces.MessageRepository {
	return &messageRepository{&sync.RWMutex{}, buffer, bSize}
}

// MarkSent mark a message as sent.
func (m messageRepository) MarkSent(code string) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	model, ok := m.buffer[code]
	if !ok {
		return fmt.Errorf("message %s not found in buffer", code)
	}
	model.IsSent = true
	m.buffer[code] = model

	return nil
}

// MarkUnSent mark a message as not sent.
func (m messageRepository) MarkUnSent(code string, attemptCount int) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	model, ok := m.buffer[code]
	if !ok {
		return fmt.Errorf("message %s not found in buffer", code)
	}
	model.IsSent = false
	model.AttemptCount = attemptCount
	m.buffer[code] = model

	return nil
}

// Save create a message in the database.
func (m messageRepository) Save(model models.Message) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	_, ok := m.buffer[model.Code]
	if ok {
		return fmt.Errorf("message %s already exist", model.Code)
	}

	if len(m.buffer) == m.bSize {
		return fmt.Errorf("buffer overflow, max size: %d", m.bSize)
	}

	m.buffer[model.Code] = model

	return nil
}

// Update modify a message in the database.
func (m messageRepository) Update(model models.Message) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	_, ok := m.buffer[model.Code]
	if !ok {
		return fmt.Errorf("message %s is not exist", model.Code)
	}
	m.buffer[model.Code] = model

	return nil
}

// Delete delete a message in the database.
func (m messageRepository) Delete(code string) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	_, ok := m.buffer[code]
	if !ok {
		return fmt.Errorf("message %s is not exist", code)
	}
	delete(m.buffer, code)

	return nil
}

// FindByCode find a message by code.
func (m messageRepository) FindByCode(code string) (*models.Message, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	model, ok := m.buffer[code]
	if !ok {
		return nil, nil
	}

	return &model, nil
}

// FindAll find messages by limit and offset.
func (m messageRepository) FindAll(attemptCountMax int, limit int, offset int) (*map[int]models.Message, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	messages := make(map[int]models.Message, limit)
	if len(m.buffer) == 0 {
		return &messages, nil
	}

	if limit <= offset {
		return &messages, fmt.Errorf("wrong limit(%d) and offset(%d)", limit, offset)
	}

	if limit > len(m.buffer) {
		limit = len(m.buffer)
	}

	if offset > len(m.buffer) {
		offset = 0
	}

	// Extract the keys into a slice
	keys := make([]string, 0, len(m.buffer))
	for key := range m.buffer {
		keys = append(keys, key)
	}

	// Sort the keys slice based on the AttemptCount field.
	sort.Slice(keys, func(i, j int) bool {
		return m.buffer[keys[i]].AttemptCount < m.buffer[keys[j]].AttemptCount
	})
	keys = keys[offset:limit]

	i := 0
	for _, key := range keys {
		if m.buffer[key].AttemptCount <= attemptCountMax {
			messages[i] = m.buffer[key]
			i++
		}
	}

	return &messages, nil
}

// IsInDatabase a message exists in the database.
func (m messageRepository) IsInDatabase(code string) (bool, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	_, ok := m.buffer[code]

	return ok, nil
}
