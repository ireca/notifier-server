package memory

import (
	"fmt"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"time"
)

type resourceRepository struct {
	buffer map[string]models.Resource
	bSize  int
}

func NewResourceRepository(buffer map[string]models.Resource, bSize int) interfaces.ResourceRepository {
	return &resourceRepository{buffer, bSize}
}

// Save creates a resource in the database.
func (r resourceRepository) Save(resource models.Resource) (int64, error) {
	_, ok := r.buffer[resource.Code]
	if ok {
		return 0, fmt.Errorf("resource %s already exist", resource.Code)
	}
	if len(r.buffer) == r.bSize {
		return 0, fmt.Errorf("buffer overflow, max size: %d", r.bSize)
	}

	resource.ID = time.Now().UnixMicro()
	r.buffer[resource.Code] = resource

	return resource.ID, nil
}

// Delete deletes a resource from the database.
func (r resourceRepository) Delete(code string) error {
	_, ok := r.buffer[code]
	if !ok {
		return fmt.Errorf("resource %s is not exist", code)
	}
	delete(r.buffer, code)

	return nil
}

// FindByCode find a resource by code.
func (r resourceRepository) FindByCode(code string) (*models.Resource, error) {
	model, ok := r.buffer[code]
	if !ok {
		return nil, nil
	}

	return &model, nil
}

// IsInDatabase a resource exists in the database.
func (r resourceRepository) IsInDatabase(code string) (bool, error) {
	_, ok := r.buffer[code]

	return ok, nil
}
