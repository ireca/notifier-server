package interfaces

import "gitlab.com/ireca/notifier-server/internal/models"
import "context"

type SettingRepository interface {
	// Save creates a user in the database.
	Save(ctx context.Context, model models.Setting) error
	// Replace replaces a user in the database.
	Replace(ctx context.Context, model models.Setting) error
	// Update updates a user in the database.
	Update(ctx context.Context, model models.Setting) error
	// Delete deletes a user in the database.
	Delete(ctx context.Context, userId int, code string) error
	// FindByCode finds a user in the database by code.
	FindByCode(ctx context.Context, userId int, code string) (*models.Setting, error)
	// FindByResource finds a user in the database by resource id.
	FindByResource(ctx context.Context, resourceId int) (*models.Setting, error)
	// FindAll finds users in the database by limit and offset.
	FindAll(ctx context.Context, userId int, limit int, offset int) (*map[int]models.Setting, error)
	// IsInDatabase user is exists in the database.
	IsInDatabase(ctx context.Context, userId int, settingId string) (bool, error)
}
