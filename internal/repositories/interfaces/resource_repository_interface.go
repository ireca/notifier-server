package interfaces

import "gitlab.com/ireca/notifier-server/internal/models"

type ResourceRepository interface {
	// Save creates a resource in the database.
	Save(model models.Resource) (int64, error)
	// Delete deletes a resource from the database.
	Delete(code string) error
	// FindByCode find a resource by code.
	FindByCode(code string) (*models.Resource, error)
	// IsInDatabase a resource exists in the database.
	IsInDatabase(code string) (bool, error)
}
