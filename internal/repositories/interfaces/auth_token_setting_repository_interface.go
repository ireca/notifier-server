package interfaces

import (
	"context"
	"github.com/jackc/pgx/v4"
)
import "gitlab.com/ireca/notifier-server/internal/models"

type AuthTokenSettingRepository interface {
	// Save creates a user in the database.
	Save(ctx context.Context, tx pgx.Tx, model models.AuthTokenSetting) error
	// Replace replaces a user in the database.
	Replace(ctx context.Context, tx pgx.Tx, model models.AuthTokenSetting) error
	// Update updates a user in the database.
	Update(ctx context.Context, tx pgx.Tx, model models.AuthTokenSetting) error
	UpdateToken(ctx context.Context, settingID int, token string) error
}
