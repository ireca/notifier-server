package interfaces

import "gitlab.com/ireca/notifier-server/internal/models"

type JournalRepository interface {
	// Save create journal record.
	Save(journal models.Journal) error
}
