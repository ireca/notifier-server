package factory

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/repositories/postgre"
)

func NewAuthTokenSettingRepository(pool *pgxpool.Pool) interfaces.AuthTokenSettingRepository {
	return postgre.NewAuthTokenSettingRepository(pool)
}
