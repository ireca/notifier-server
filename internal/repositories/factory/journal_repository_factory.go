package factory

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/repositories/postgre"
)

func NewJournalRepository(context context.Context, pool *pgxpool.Pool) interfaces.JournalRepository {
	return postgre.NewJournalRepository(context, pool)
}
