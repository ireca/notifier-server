package factory

import (
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/repositories/memory"
)

func NewInstantlyMessageRepository(buffer map[string]models.Message, bSize int) interfaces.MessageRepository {
	return memory.NewMessageRepository(buffer, bSize)
}
