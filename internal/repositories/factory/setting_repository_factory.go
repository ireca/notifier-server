package factory

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/repositories/postgre"
)

func NewSettingRepository(pool *pgxpool.Pool, authTokenRep interfaces.AuthTokenSettingRepository) interfaces.SettingRepository {
	return postgre.NewSettingRepository(pool, authTokenRep)
}
