package factory

import (
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/repositories/memory"
)

func NewInstantlyResourceRepository(buffer map[string]models.Resource, bSize int) interfaces.ResourceRepository {
	return memory.NewResourceRepository(buffer, bSize)
}
