package postgre

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
)

type messageRepository struct {
	context    context.Context
	connection *pgxpool.Pool
}

func NewMessageRepository(context context.Context, pool *pgxpool.Pool) interfaces.MessageRepository {
	return &messageRepository{context, pool}
}

// MarkSent mark a message as sent.
func (u messageRepository) MarkSent(code string) error {
	_, err := u.connection.Exec(
		u.context,
		`UPDATE 
               ns_messages 
             SET 
               is_sent=True,
               updated_at=NOW() 
             WHERE 
               code=$1 AND deleted_at IS NULL`,
		code,
	)
	if err != nil {
		return err
	}

	return nil
}

// MarkUnSent mark a message as not sent.
func (u messageRepository) MarkUnSent(code string, attemptCount int) error {
	_, err := u.connection.Exec(
		u.context,
		`UPDATE 
               ns_messages 
             SET 
               is_sent=False,
               attempt_count=$1,
               updated_at=NOW() 
             WHERE 
               code=$2 AND deleted_at IS NULL`,
		attemptCount, code,
	)
	if err != nil {
		return err
	}

	return nil
}

// Save create a message in the database.
func (u messageRepository) Save(model models.Message) error {
	var lastInsertId int
	err := u.connection.QueryRow(
		u.context,
		`INSERT INTO ns_messages(code, user_id, resource_id, command, priority, content, is_sent, attempt_count, 
				is_sent_callback, callback_attempt_count, description, send_at, success_http_status,
               success_response, headers, urlParams) 
             VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16) RETURNING id`,
		&model.Code, &model.UserId, &model.ResourceId, &model.Command, &model.Priority, &model.Content, &model.IsSent,
		&model.AttemptCount, &model.IsSentCallback, &model.CallbackAttemptCount, &model.Description, &model.SendAt,
		&model.SuccessHttpStatus, &model.SuccessResponse, &model.Headers, &model.URLParams,
	).Scan(&lastInsertId)
	if err != nil {
		return err
	}

	return err
}

// Update modify a message in the database.
func (u messageRepository) Update(model models.Message) error {
	_, err := u.connection.Exec(
		u.context,
		`UPDATE 
               ns_messages 
             SET 
               user_id=$1, 
               resource_id=$2, 
               command=$3, 
               priority=$4, 
               content=$5, 
               is_sent=$6, 
               attempt_count=$7, 
               is_sent_callback=$8, 
               callback_attempt_count=$9, 
               description=$10, 
               send_at=$11, 
               success_http_status=$12,
               success_response=$13,
               headers=$14, 
               urlParams=$15,
               updated_at=NOW() 
             WHERE 
               code=$14 AND deleted_at IS NULL`,
		&model.UserId, &model.ResourceId, &model.Command, &model.Priority, &model.Content, &model.IsSent,
		&model.AttemptCount, &model.IsSentCallback, &model.CallbackAttemptCount, &model.Description, &model.SendAt,
		&model.SuccessHttpStatus, &model.SuccessResponse, &model.Code, &model.Headers, &model.URLParams,
	)
	if err != nil {
		return err
	}

	return nil
}

// Delete delete a message in the database.
func (u messageRepository) Delete(code string) error {
	_, err := u.connection.Exec(u.context, "UPDATE ns_messages SET deleted_at=NOW() WHERE code=$1 AND deleted_at IS NULL", code)

	return err
}

// FindByCode find a message by code.
func (u messageRepository) FindByCode(code string) (*models.Message, error) {
	var model models.Message
	err := u.connection.QueryRow(
		u.context,
		`SELECT r.url, r.description, m.id, m.code, m.user_id, m.resource_id, m.command, m.priority, m.content, m.is_sent, m.attempt_count, 
				m.is_sent_callback, m.callback_attempt_count, m.description, m.send_at, m.success_http_status, 
                m.success_response, m.created_at, m.headers, m.urlParams,
                COALESCE(s.code, ''), COALESCE(s.title, ''), COALESCE(s.callback_url, ''), COALESCE(s.count,0), COALESCE(s.intervals,'{}'), 
                COALESCE(s.timeout, 0), COALESCE(s.description, '')
             FROM 
               ns_messages m
             LEFT JOIN ns_resources r ON r.id=m.resource_id 
             LEFT JOIN ns_settings s ON s.resource_id=m.resource_id 
             WHERE 
               m.code=$1 AND m.deleted_at IS NULL`,
		code,
	).Scan(&model.Resource.URL, &model.Resource.Description, &model.ID, &model.Code, &model.UserId, &model.ResourceId, &model.Command, &model.Priority,
		&model.Content, &model.IsSent, &model.AttemptCount, &model.IsSentCallback, &model.CallbackAttemptCount,
		&model.Description, &model.SendAt, &model.SuccessHttpStatus, &model.SuccessResponse, &model.CreatedAt,
		&model.Headers, &model.URLParams,
		&model.Resource.Setting.Code, &model.Resource.Setting.Title, &model.Resource.Setting.CallbackURL,
		&model.Resource.Setting.Count, &model.Resource.Setting.Intervals, &model.Resource.Setting.Timeout,
		&model.Resource.Setting.Description,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &model, nil
}

// FindAll find messages by limit and offset.
func (u messageRepository) FindAll(attemptCountMax int, limit int, offset int) (*map[int]models.Message, error) {
	rows, err := u.connection.Query(
		u.context,
		`SELECT 
                r.id, r.url, r.code, m.id, m.code, m.user_id, m.resource_id, m.command, m.priority, m.content, m.is_sent, m.attempt_count, 
				m.is_sent_callback, m.callback_attempt_count, m.description, m.send_at, m.success_http_status, m.success_response, 
                m.created_at, m.headers, m.urlParams, u.id, u.code, u.role, u.title, u.auth_token,
                s.id, s.code, s.title, s.count, s.intervals, s.timeout, s.callback_url, s.success_http_statuses, s.auth_type,
                nts.id, nts.setting_id, nts.token, nts.url, nts.headers, nts.body, nts.check_token_http_codes, nts.check_token_sub_response
             FROM 
               ns_messages m
             LEFT JOIN ns_resources r ON r.id=m.resource_id 
             LEFT JOIN ns_users u ON u.id=m.user_id 
             LEFT JOIN ns_settings s ON s.user_id=m.user_id AND s.resource_id is null
             LEFT JOIN ns_token_settings nts ON nts.setting_id=s.id
             WHERE 
  				m.is_sent IS False AND m.deleted_at IS NULL AND m.attempt_count<=$1
			ORDER BY m.created_at DESC, m.attempt_count ASC
			LIMIT $2 OFFSET $3`,
		attemptCountMax, limit, offset,
	)
	if err != nil {
		return nil, err
	}

	users, err := getMessageModels(rows)
	if err != nil {
		return nil, err
	}

	return &users, nil
}

func getMessageModels(rows pgx.Rows) (map[int]models.Message, error) {
	var messages = make(map[int]models.Message)
	for rows.Next() {
		var model models.Message
		model.Resource = models.Resource{}
		model.User = models.User{}
		err := rows.Scan(
			&model.Resource.ID, &model.Resource.URL, &model.Resource.Code, &model.ID, &model.Code, &model.UserId, &model.ResourceId, &model.Command, &model.Priority,
			&model.Content, &model.IsSent, &model.AttemptCount, &model.IsSentCallback, &model.CallbackAttemptCount,
			&model.Description, &model.SendAt, &model.SuccessHttpStatus, &model.SuccessResponse, &model.CreatedAt,
			&model.Headers, &model.URLParams,
			&model.User.ID, &model.User.Code, &model.User.Role, &model.User.Title, &model.User.AuthToken,
			&model.Resource.Setting.ID, &model.Resource.Setting.Code, &model.Resource.Setting.Title,
			&model.Resource.Setting.Count, &model.Resource.Setting.Intervals, &model.Resource.Setting.Timeout,
			&model.Resource.Setting.CallbackURL, &model.Resource.Setting.SuccessHTTPStatuses, &model.Resource.Setting.AuthType,
			&model.Resource.Setting.Auth.ID, &model.Resource.Setting.Auth.SettingId, &model.Resource.Setting.Auth.Token, &model.Resource.Setting.Auth.URL,
			&model.Resource.Setting.Auth.Headers, &model.Resource.Setting.Auth.Body, &model.Resource.Setting.Auth.CheckTokenHTTPCodes,
			&model.Resource.Setting.Auth.CheckTokenSubResponse,
		)
		if err != nil {
			return nil, err
		}
		model.Resource.UserId = model.User.ID
		messages[model.ID] = model
	}

	return messages, nil
}

// IsInDatabase a message exists in the database.
func (u messageRepository) IsInDatabase(code string) (bool, error) {
	model, err := u.FindByCode(code)

	return !(model == nil), err
}
