package postgre

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
)

type authTokenSettingRepository struct {
	connection *pgxpool.Pool
}

func NewAuthTokenSettingRepository(pool *pgxpool.Pool) interfaces.AuthTokenSettingRepository {
	return &authTokenSettingRepository{pool}
}

func (a authTokenSettingRepository) Save(ctx context.Context, tx pgx.Tx, model models.AuthTokenSetting) error {
	var lastInsertId int
	err := tx.QueryRow(
		ctx,
		`INSERT INTO ns_token_settings(
			   setting_id, 
			   token, 
			   url, 
			   headers, 
			   body, 
			   check_token_http_codes, 
               check_token_sub_response,
			   description
             ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id`,
		&model.SettingId,
		&model.Token,
		&model.URL,
		&model.Headers,
		&model.Body,
		&model.CheckTokenHTTPCodes,
		&model.CheckTokenSubResponse,
		&model.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return err
	}

	return err
}

func (a authTokenSettingRepository) Replace(ctx context.Context, tx pgx.Tx, model models.AuthTokenSetting) error {
	var lastInsertId int
	err := tx.QueryRow(
		ctx,
		`INSERT INTO 
               ns_token_settings (
			     setting_id, 
			     token, 
			     url, 
			     headers, 
			     body, 
			     check_token_http_codes, 
                 check_token_sub_response,
			     description                                  
               )
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			ON CONFLICT (setting_id) DO UPDATE 
			SET
			  token = excluded.token,
			  url = excluded.url,
			  headers = excluded.headers,
			  body = excluded.body,
              check_token_http_codes = excluded.check_token_http_codes,
              check_token_sub_response = excluded.check_token_sub_response,
              description = excluded.description,
              updated_at=NOW()
            RETURNING id`,
		&model.SettingId,
		&model.Token,
		&model.URL,
		&model.Headers,
		&model.Body,
		&model.CheckTokenHTTPCodes,
		&model.CheckTokenSubResponse,
		&model.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return err
	}

	return err
}

func (a authTokenSettingRepository) Update(ctx context.Context, tx pgx.Tx, model models.AuthTokenSetting) error {
	_, err := tx.Exec(
		ctx,
		`UPDATE 
               ns_token_settings 
             SET 
			  token=$1, 
			  url=$2, 
			  headers=$3, 
			  body=$4, 
			  check_token_http_codes=$5,
			  check_token_sub_response=$6,
			  description=$7,   
              updated_at=NOW() 
             WHERE 
               setting_id=$8`,
		&model.Token,
		&model.URL,
		&model.Headers,
		&model.Body,
		&model.CheckTokenHTTPCodes,
		&model.CheckTokenSubResponse,
		&model.Description,
		&model.SettingId,
	)
	if err != nil {
		return err
	}

	return nil
}

func (a authTokenSettingRepository) UpdateToken(ctx context.Context, settingID int, token string) error {
	_, err := a.connection.Exec(
		ctx,
		`UPDATE 
          ns_token_settings 
        SET
          token=$1
        WHERE
          setting_id=$2`,
		&token,
		&settingID,
	)
	if err != nil {
		return err
	}

	return nil
}
