package postgre

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
)

type settingRepository struct {
	connection   *pgxpool.Pool
	authTokenRep interfaces.AuthTokenSettingRepository
}

func NewSettingRepository(pool *pgxpool.Pool, authTokenRep interfaces.AuthTokenSettingRepository) interfaces.SettingRepository {
	return &settingRepository{pool, authTokenRep}
}

// Save creates a setting in the database.
func (s settingRepository) Save(ctx context.Context, model models.Setting) error {
	tx, err := s.connection.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}
	defer func() error {
		if err != nil {
			return tx.Rollback(ctx)
		} else {
			return tx.Commit(ctx)
		}
	}()

	var lastInsertId int
	err = tx.QueryRow(
		ctx,
		`INSERT INTO ns_settings (
				 code, 
				 user_id, 
				 resource_id, 
				 title, 
				 callback_url,
				 count, 
				 intervals, 
				 timeout, 
                 success_http_statuses,
                 auth_type,        
				 description
        	)
            VALUES ($1, $2, NULLIF($3,0), $4, $5, $6, $7, $8, $9, $10, $11) RETURNING id`,
		&model.Code, &model.UserId, &model.ResourceId, &model.Title, &model.CallbackURL, &model.Count, &model.Intervals,
		&model.Timeout, &model.SuccessHTTPStatuses, &model.AuthType, &model.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return err
	}

	model.Auth.SettingId = lastInsertId
	err = s.authTokenRep.Save(ctx, tx, model.Auth)
	if err != nil {
		return err
	}

	return err
}

// Update updates a setting in the database.
func (s settingRepository) Update(ctx context.Context, model models.Setting) error {
	tx, err := s.connection.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}
	defer func() error {
		if err != nil {
			return tx.Rollback(ctx)
		} else {
			return tx.Commit(ctx)
		}
	}()

	_, err = tx.Exec(
		ctx,
		`UPDATE 
               ns_settings 
             SET 
               title=$1, 
               callback_url=$2, 
               count=$3, 
               intervals=$4, 
               timeout=$5, 
               success_http_statuses=$6,
               auth_type=$7,                  
               description=$8, 
               updated_at=NOW() 
             WHERE 
               user_id=$9 AND code=$10 AND deleted_at IS NULL`,
		&model.Title, &model.CallbackURL, &model.Count, &model.Intervals, &model.Timeout, &model.SuccessHTTPStatuses,
		&model.AuthType, &model.Description,
		&model.UserId, &model.Code,
	)
	if err != nil {
		return err
	}

	err = s.authTokenRep.Update(ctx, tx, model.Auth)
	if err != nil {
		return err
	}

	return nil
}

// Replace replaces a setting in the database.
func (s settingRepository) Replace(ctx context.Context, model models.Setting) error {
	tx, err := s.connection.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}
	defer func() error {
		if err != nil {
			return tx.Rollback(ctx)
		} else {
			return tx.Commit(ctx)
		}
	}()

	var lastInsertId int
	err = tx.QueryRow(
		ctx,
		`INSERT INTO 
               ns_settings (
				 code, 
				 user_id, 
				 resource_id, 
				 title, 
				 callback_url, 
				 count, 
				 intervals, 
				 timeout, 
                 success_http_statuses,
                 auth_type,                                 
				 description
               )
             VALUES ($1, $2, NULLIF($3,0), $4, $5, $6, $7, $8, $9, $10, $11)
			ON CONFLICT (user_id, resource_id, code) DO UPDATE 
			SET
			  title = excluded.title,
			  callback_url = excluded.callback_url,
			  count = excluded.count,
			  intervals = excluded.intervals,
              timeout = excluded.timeout,
              success_http_statuses = excluded.success_http_statuses,
              auth_type = excluded.auth_type,              
              description = excluded.description,
              updated_at=NOW()
             RETURNING id`,
		&model.Code, &model.UserId, &model.ResourceId, &model.Title, &model.CallbackURL, &model.Count, &model.Intervals,
		&model.Timeout, &model.SuccessHTTPStatuses, &model.AuthType, &model.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return err
	}

	model.Auth.SettingId = lastInsertId
	err = s.authTokenRep.Replace(ctx, tx, model.Auth)
	if err != nil {
		return err
	}

	return err
}

// Delete deletes a setting in the database.
func (s settingRepository) Delete(ctx context.Context, userId int, code string) error {
	_, err := s.connection.Exec(
		ctx,
		"UPDATE ns_settings SET deleted_at=NOW() WHERE user_id=$1 AND code=$2 AND deleted_at IS NULL",
		&userId, &code,
	)

	return err
}

// FindByCode finds a setting in the database by code.
func (s settingRepository) FindByCode(ctx context.Context, userId int, code string) (*models.Setting, error) {
	var model models.Setting
	err := s.connection.QueryRow(
		ctx,
		`SELECT 
               s.id, 
               s.code, 
               s.user_id, 
               COALESCE(s.resource_id, 0), 
               s.title, 
               s.callback_url, 
               s.count, 
               s.intervals, 
               s.timeout, 
               s.description, 
               s.created_at,
               COALESCE(s.updated_at, '0001-01-01 00:00:00 +0000'),
               COALESCE(ts.setting_id, 0),
               COALESCE(ts.token, ''),
               COALESCE(ts.url, ''),
               COALESCE(ts.headers, '{}'),
               COALESCE(ts.body, ''),
               COALESCE(ts.check_token_http_codes, '{}'),
               COALESCE(ts.check_token_sub_response, ''),
               COALESCE(ts.description, ''),
               COALESCE(ts.created_at, '0001-01-01 00:00:00 +0000'),
               COALESCE(ts.updated_at, '0001-01-01 00:00:00 +0000')
             FROM 
               ns_settings s
             LEFT JOIN ns_token_settings ts on s.id = ts.setting_id
             WHERE 
               user_id=$1 AND code=$2 AND deleted_at IS NULL`,
		userId, code,
	).Scan(&model.ID, &model.Code, &model.UserId, &model.ResourceId, &model.Title, &model.CallbackURL, &model.Count,
		&model.Intervals, &model.Timeout, &model.Description, &model.CreatedAt, &model.UpdatedAt,
		&model.Auth.SettingId, &model.Auth.Token, &model.Auth.URL, &model.Auth.Headers, &model.Auth.Body,
		&model.Auth.CheckTokenHTTPCodes, &model.Auth.CheckTokenSubResponse, &model.Auth.Description,
		&model.Auth.CreatedAt, &model.Auth.UpdatedAt,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &model, nil
}

// FindByResource finds a setting in the database by resource id.
func (s settingRepository) FindByResource(ctx context.Context, resourceId int) (*models.Setting, error) {
	var model models.Setting
	err := s.connection.QueryRow(
		ctx,
		`SELECT 
               s.id, 
               s.code, 
               s.user_id, 
               COALESCE(s.resource_id, 0), 
               s.title, 
               s.callback_url, 
               s.count, 
               s.intervals, 
               s.timeout, 
               s.description, 
               s.created_at,
               COALESCE(s.updated_at, '0001-01-01 00:00:00 +0000'),
               COALESCE(ts.setting_id, 0),
               COALESCE(ts.token, ''),
               COALESCE(ts.url, ''),
               COALESCE(ts.headers, '{}'),
               COALESCE(ts.body, ''),
               COALESCE(ts.check_token_http_codes, '{}'),
               COALESCE(ts.check_token_sub_response, ''),
               COALESCE(ts.description, ''),
               COALESCE(ts.created_at, '0001-01-01 00:00:00 +0000'),
               COALESCE(ts.updated_at, '0001-01-01 00:00:00 +0000')         
             FROM 
               ns_settings s 
             LEFT JOIN ns_token_settings ts on s.id = ts.setting_id
             WHERE 
               resource_id=$1 AND code=$2 AND deleted_at IS NULL`,
		resourceId,
	).Scan(&model.ID, &model.Code, &model.UserId, &model.ResourceId, &model.Title, &model.CallbackURL, &model.Count,
		&model.Intervals, &model.Timeout, &model.Description, &model.CreatedAt, &model.UpdatedAt,
		&model.Auth.SettingId, &model.Auth.Token, &model.Auth.URL, &model.Auth.Headers, &model.Auth.Body,
		&model.Auth.CheckTokenHTTPCodes, &model.Auth.CheckTokenSubResponse, &model.Auth.Description,
		&model.Auth.CreatedAt, &model.Auth.UpdatedAt,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &model, nil
}

// FindAll finds users in the database by limit and offset.
func (s settingRepository) FindAll(ctx context.Context, userId int, limit int, offset int) (*map[int]models.Setting, error) {
	rows, err := s.connection.Query(
		ctx,
		`SELECT 
				s.id, 
				s.code, 
				s.user_id, 
				COALESCE(s.resource_id, 0), 
				s.title, 
				s.callback_url, 
				s.count, 
				s.intervals, 
				s.timeout, 
				s.description, 
				s.created_at,
				COALESCE(s.updated_at, '0001-01-01 00:00:00 +0000'),
                COALESCE(s.updated_at, '0001-01-01 00:00:00 +0000'),
                COALESCE(ts.setting_id, 0),
                COALESCE(ts.token, ''),
                COALESCE(ts.url, ''),
                COALESCE(ts.headers, '{}'),
                COALESCE(ts.body, ''),
                COALESCE(ts.check_token_http_codes, '{}'),
                COALESCE(ts.check_token_sub_response, ''),
                COALESCE(ts.description, ''),
                COALESCE(ts.created_at, '0001-01-01 00:00:00 +0000'),
                COALESCE(ts.updated_at, '0001-01-01 00:00:00 +0000')       			
			FROM ns_settings s
			LEFT JOIN ns_token_settings ts on s.id = ts.setting_id
			WHERE 
  				s.user_id=$1 AND s.deleted_at IS NULL 
			ORDER BY s.id
			LIMIT $2 OFFSET $3`,
		userId, limit, offset,
	)
	if err != nil {
		return nil, err
	}

	items, err := getSettingModels(rows)
	if err != nil {
		return nil, err
	}

	return &items, nil
}

// getModels returns array of the setting models.
func getSettingModels(rows pgx.Rows) (map[int]models.Setting, error) {
	var settings = make(map[int]models.Setting)
	var model models.Setting
	for rows.Next() {
		err := rows.Scan(
			&model.ID, &model.Code, &model.UserId, &model.ResourceId, &model.Title, &model.CallbackURL, &model.Count,
			&model.Intervals, &model.Timeout, &model.Description, &model.CreatedAt, &model.UpdatedAt,
			&model.Auth.SettingId, &model.Auth.Token, &model.Auth.URL, &model.Auth.Headers, &model.Auth.Body,
			&model.Auth.CheckTokenHTTPCodes, &model.Auth.CheckTokenSubResponse, &model.Auth.Description,
			&model.Auth.CreatedAt, &model.Auth.UpdatedAt,
		)
		if err != nil {
			return nil, err
		}
		settings[model.ID] = model
	}

	return settings, nil
}

// IsInDatabase setting is exists in the database.
func (s settingRepository) IsInDatabase(ctx context.Context, userId int, code string) (bool, error) {
	model, err := s.FindByCode(ctx, userId, code)

	return !(model == nil), err
}
