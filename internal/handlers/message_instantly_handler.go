package handlers

import (
	"encoding/json"
	"errors"
	"github.com/wlMalk/gapi/constants"
	"gitlab.com/ireca/notifier-server/internal/services"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"log"
	"net/http"
)

// CreateInstantlyMessageHandler create a message handler.
func CreateInstantlyMessageHandler(r *http.Request, w http.ResponseWriter, param services.MessageRouteParameters) {
	httpRequests, err := getMessageRequests(r)
	log.Printf("create_instantly_message_request: %v", httpRequests)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ms := services.NewMessageService(param)
	responses, err := ms.CreateMessages(r.Context(), *httpRequests)
	if err != nil {
		if errors.Is(err, services.AlreadyExistError) {
			http.Error(w, err.Error(), http.StatusConflict)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", constants.MIME_JSON)
	w.WriteHeader(http.StatusCreated)
	jsonResponse, err := json.Marshal(responses)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.LogErr(w.Write(jsonResponse))
}

// DeletedInstantlyMessageHandler Delete message handler.
func DeletedInstantlyMessageHandler(r *http.Request, w http.ResponseWriter, param services.MessageRouteParameters) {
	httpRequest, err := getMessageDeleteRequest(r)
	log.Printf("delete_instantly_message_request: %v", httpRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ms := services.NewMessageService(param)
	err = ms.DeleteMessage(r.Context(), *httpRequest)
	if err != nil {
		if errors.Is(err, services.RequestError) {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else if errors.Is(err, services.NotFoundError) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", constants.MIME_JSON)
	w.WriteHeader(http.StatusAccepted)
}

// GetInstantlyMessageHandler get message handler.
func GetInstantlyMessageHandler(r *http.Request, w http.ResponseWriter, param services.MessageRouteParameters) {
	httpRequest := getMessageRequest(r)
	log.Printf("get_instantly_message_request: %v", httpRequest)
	ms := services.NewMessageService(param)
	response, err := ms.GetMessage(r.Context(), *httpRequest)
	if err != nil {
		if errors.Is(err, services.RequestError) {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else if errors.Is(err, services.NotFoundError) {
			http.Error(w, err.Error(), http.StatusNoContent)
			return
		}

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", constants.MIME_JSON)
	w.WriteHeader(200)
	jsonResponse, err := getMessageJsonResponse(*response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	utils.LogErr(w.Write(jsonResponse))
}
