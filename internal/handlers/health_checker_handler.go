package handlers

import (
	"fmt"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"net/http"
)

// HealthCheckerHandler check health of the service.
func HealthCheckerHandler(r *http.Request, w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	_, err := fmt.Fprintf(w, "Service is healthy\n")
	if err != nil {
		utils.LogError("can't print message: %v", err)
	}
}
