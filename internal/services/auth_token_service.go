package services

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	sInterfaces "gitlab.com/ireca/notifier-server/internal/services/interfaces"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

type authTokenService struct {
	cfg      config.Config
	atsRep   interfaces.AuthTokenSettingRepository
	rService sInterfaces.ResourceServiceInterface
	jRep     interfaces.JournalRepository
}

func NewAuthTokenService(cfg config.Config, atsRep interfaces.AuthTokenSettingRepository, rService sInterfaces.ResourceServiceInterface, jRep interfaces.JournalRepository) sInterfaces.AuthTokenServiceInterface {
	return authTokenService{cfg, atsRep, rService, jRep}
}

func (a authTokenService) UpdateToken(ctx context.Context, userId int, aSetting models.AuthTokenSetting) error {
	model := models.Resource{
		UserId:      userId,
		URL:         aSetting.URL,
		Description: aSetting.Description,
	}
	resId, err := a.rService.CreateIfNotExist(model)
	if err != nil {
		return fmt.Errorf("can't create a resource: %s", err.Error())
	}

	token, err := a.getToken(userId, resId, aSetting)
	if err != nil {
		return fmt.Errorf("i can't get token: %s", err.Error())
	}

	err = a.atsRep.UpdateToken(ctx, aSetting.SettingId, token)
	if err != nil {
		return fmt.Errorf("i can't get token: %s", err.Error())
	}

	return nil
}

type tokenResponse struct {
	AccessToken      string `json:"access_token,omitempty"`
	TokenType        string `json:"token_type,omitempty"`
	ExpiresIn        int    `json:"expires_in,omitempty"`
	Error            string `json:"error,omitempty"`
	ErrorDescription string `json:"error_description,omitempty"`
	ErrorURI         string `json:"error_uri,omitempty"`
}

func (a authTokenService) getToken(userId int, resId int64, aSetting models.AuthTokenSetting) (string, error) {
	response := tokenResponse{}
	sts, cnt, err := a.sendPOSTRequest(aSetting)
	if err != nil {
		return "", fmt.Errorf("can't get token: %s", err.Error())
	}

	err = json.Unmarshal(cnt, &response)
	if err != nil {
		return "", fmt.Errorf("can't decode json response: %s, %s", err.Error(), cnt)
	}

	journal := models.Journal{
		UserId:          userId,
		ResourceId:      resId,
		ResponseStatus:  sts,
		ResponseContent: string(cnt),
	}
	err = a.jRep.Save(journal)
	if err != nil {
		utils.LogError("can't write a record to the journal: %v", err)
	}

	if response.Error != "" {
		return "", fmt.Errorf("can't get token: %s", response.Error)
	}

	log.Println("OAuth2 token was received!")

	return response.AccessToken, nil
}

// sendRequest send a request from a message.
func (a authTokenService) sendPOSTRequest(aSetting models.AuthTokenSetting) (status int, content []byte, err error) {
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Timeout: time.Duration(a.cfg.Sender.Timeout) * time.Second,
	}

	req, err := http.NewRequest("POST", aSetting.URL, strings.NewReader(aSetting.Body))
	if err != nil {
		return 0, []byte(""), err
	}

	const headerKeyIndex = 0
	const headerValueIndex = 1
	for _, header := range aSetting.Headers {
		hParts := strings.Split(header, ":")
		if len(hParts) < 2 {
			utils.LogError("wrong header: %s", header)
			continue
		}
		req.Header.Add(hParts[headerKeyIndex], hParts[headerValueIndex])
	}
	resp, err := client.Do(req)
	if err != nil {
		return 0, []byte(""), err
	}
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, []byte(""), err
	}
	defer utils.ResourceClose(resp.Body)

	return resp.StatusCode, respBody, nil
}
