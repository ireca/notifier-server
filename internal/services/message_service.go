package services

import (
	"context"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/jinzhu/copier"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/services/auth"
	"hash/fnv"
	"strconv"
	"time"
)

type MessServiceInterface interface {
	CreateMessages(ctx context.Context, messageRequests []MessageCreateRequest) ([]MessageCreateResponse, error)
	DeleteMessage(ctx context.Context, r MessageDeleteRequest) error
	GetMessage(ctx context.Context, httpRequest MessageGetRequest) (*MessageResponse, error)
}

type messageService struct {
	param MessageRouteParameters
}

func NewMessageService(param MessageRouteParameters) MessServiceInterface {
	return messageService{param}
}

type MessageCreateRequest struct {
	MessageId               string                 `validate:"required,max=64" faker:"uuid_hyphenated" json:"messageId"`
	Priority                string                 `validate:"required,oneof='high' 'low' 'normal'" faker:"oneof: high,low,normal" json:"priority,omitempty"`
	ResourceId              string                 `validate:"required_without=Resource" faker:"-" json:"resourceId,omitempty"`
	Resource                *ResourceCreateRequest `validate:"required_without=ResourceId" json:"resource,omitempty"`
	Headers                 []string               `validate:"min=0,max=10,dive,max=150" faker:"slice_len=10" json:"headers,omitempty"`
	URLParams               []string               `validate:"min=0,max=10,dive,max=150" faker:"slice_len=10" json:"urlParams,omitempty"`
	Command                 string                 `validate:"required,max=10" faker:"oneof: post,put" json:"command,omitempty"`
	Content                 string                 `validate:"-" faker:"len=20" json:"content,omitempty"`
	SendAt                  string                 `validate:"-" faker:"timestamp" json:"sendAt,omitempty"`
	SuccessHttpStatus       int                    `validate:"numeric" faker:"oneof: 200,201,204" json:"successHttpStatus,omitempty"`
	SuccessResponse         string                 `validate:"max=300" faker:"len=10" json:"successResponse,omitempty"`
	Description             string                 `validate:"max=100" faker:"len=100" json:"description,omitempty"`
	isSendNotReceivedNotify bool
}

type ResourceCreateRequest struct {
	URL         string               `validate:"required,max=1000" faker:"url" json:"url,omitempty"`
	Description string               `validate:"max=100" faker:"len=100" json:"description,omitempty"`
	Setting     SettingCreateRequest `validate:"-" json:"setting,omitempty"`
}

const (
	ErrorCode   = 1
	SuccessCode = 0
)

type MessageCreateResponse struct {
	MessageID  string `json:"messageId"`
	ResourceID string `json:"resourceId"`
	Code       int    `json:"code"`
	Message    string `json:"message"`
	err        error
}

type MessageRouteParameters struct {
	Config             config.Config
	UserRepository     interfaces.UserRepository
	ResourceRepository interfaces.ResourceRepository
	MessageRepository  interfaces.MessageRepository
	SettingRepository  interfaces.SettingRepository
}

// CreateMessages create messages by request.
func (m messageService) CreateMessages(ctx context.Context, messageRequests []MessageCreateRequest) ([]MessageCreateResponse, error) {
	var responses []MessageCreateResponse
	validate := validator.New()
	userAuth := getUserAuth(ctx)
	for _, messageRequest := range messageRequests {
		response := MessageCreateResponse{MessageID: messageRequest.MessageId, Code: ErrorCode}
		messageRequest = bringValues(messageRequest)
		err := validate.Struct(messageRequest)
		if err != nil {
			return nil, fmt.Errorf("this request has mistake: %w", err)
		}

		isExist, err := m.param.MessageRepository.IsInDatabase(messageRequest.MessageId)
		if err != nil {
			return nil, fmt.Errorf("can't get user from the database: %w", err)
		}
		if isExist {
			return nil, fmt.Errorf("%w, messageId=: %s", AlreadyExistError, messageRequest.MessageId)
		}

		resourceId := int64(0)
		resourceCode := ""
		var resource *models.Resource
		if messageRequest.ResourceId != "" {
			resource, err = m.param.ResourceRepository.FindByCode(messageRequest.ResourceId)
			if err != nil {
				return nil, fmt.Errorf("can't get resource from the database: %w", err)
			} else if resource == nil {
				return nil, fmt.Errorf("resource not found in the database: %w", err)
			}
			resourceId = resource.ID
			resourceCode = resource.Code
		} else {
			resourceCode, err = getHash(messageRequest.Resource.URL)
			if err != nil {
				return nil, fmt.Errorf("can't generate hash by resource url: %w", err)
			}
			resource, err = m.param.ResourceRepository.FindByCode(resourceCode)
			if err != nil {
				return nil, fmt.Errorf("can't get resource from the database: %w", err)
			}
			if resource == nil {
				resourceId, err = m.param.ResourceRepository.Save(models.Resource{
					UserId:      userAuth.User.ID,
					Code:        resourceCode,
					URL:         messageRequest.Resource.URL,
					Description: messageRequest.Resource.Description,
				})
				if err != nil {
					return nil, fmt.Errorf("can't create resource in the database: %w", err)
				}
			} else {
				resourceId = resource.ID
			}

			if messageRequest.Resource.Setting.SettingId != "" {
				var setting models.Setting
				err = copier.Copy(&setting, &messageRequest.Resource.Setting)
				if err != nil {
					return nil, fmt.Errorf("can't copy data from the request: %w", err)
				}
				setting.UserId = userAuth.User.ID
				setting.ResourceId = resourceId
				err = m.param.SettingRepository.Replace(ctx, setting)
				if err != nil {
					return nil, fmt.Errorf("can't replace a setting in the database: %w", err)
				}
			}
		}

		var message models.Message
		err = copier.Copy(&message, &messageRequest)
		message.UserId = userAuth.User.ID
		message.ResourceId = resourceId
		if err != nil {
			return nil, fmt.Errorf("can't copy data from the request: %w", err)
		}
		if messageRequest.SendAt != "" {
			message.SendAt, err = getTimeFromStr(messageRequest.SendAt)
			if err != nil {
				return nil, fmt.Errorf("request has wrong sentAt value: %w", err)
			}
		}

		err = m.param.MessageRepository.Save(message)
		if err != nil {
			return nil, fmt.Errorf("can't create a message in the database: %w", err)
		}

		response.ResourceID = resourceCode
		response.Code = SuccessCode
		response.Message = "Ok"
		responses = append(responses, response)
	}

	return responses, nil
}

func bringValues(request MessageCreateRequest) MessageCreateRequest {
	if request.Priority == "" {
		request.Priority = "normal"
	}
	if request.Command == "" {
		request.Command = "POST"
	}
	if request.SendAt == "" {
		now := time.Now()
		request.SendAt = now.Format("2006-01-02 15:04:05")
	}
	if request.SuccessHttpStatus == 0 {
		request.SuccessHttpStatus = 201
	}

	return request
}

// getHash generate hash for the resource.
func getHash(s string) (string, error) {
	h := fnv.New64()
	_, err := h.Write([]byte(s))
	if err != nil {
		return "", fmt.Errorf("can't generate hash: %w", err)
	}

	return strconv.FormatUint(h.Sum64(), 10), nil
}

type MessageDeleteRequest struct {
	MessageId string `json:"messageId" validate:"required,max=64"`
}

// DeleteMessage delete a message by request.
func (m messageService) DeleteMessage(ctx context.Context, r MessageDeleteRequest) error {
	validate := validator.New()
	err := validate.Struct(r)
	if err != nil {
		return fmt.Errorf("this request has mistake: %w", err)
	}

	//userAuth := getUserAuth(ctx)
	isExist, err := m.param.MessageRepository.IsInDatabase(r.MessageId)
	if err != nil {
		return fmt.Errorf("can't get message from the database: %w", err)
	}
	if !isExist {
		return fmt.Errorf("%w, messageId=%s", NotFoundError, r.MessageId)
	}

	err = m.param.MessageRepository.Delete(r.MessageId)
	if err != nil {
		return fmt.Errorf("can't delete an message from the database: %w", err)
	}

	return nil
}

type MessageGetRequest struct {
	MessageId string `validate:"required,min=1,max=64"`
}

type MessageResponse struct {
	MessageId               string           `json:"messageId"`
	Priority                string           `json:"priority"`
	Resource                ResourceResponse `validate:"required" json:"resource,omitempty"`
	Command                 string           `json:"command"`
	Content                 string           `json:"content"`
	SendAt                  string           `json:"sendAt"`
	SuccessHttpStatus       int              `json:"successHttpStatus"`
	SuccessResponse         string           `json:"successResponse"`
	Description             string           `json:"description"`
	IsSendNotReceivedNotify bool             `json:"isSendNotReceivedNotify"`
	IsSent                  bool             `json:"isSent"`
	AttemptCount            int              `json:"attemptCount"`
	IsSentCallback          bool             `json:"isSentCallback"`
	CallbackAttemptCount    int              `json:"callbackAttemptCount"`
	CreatedAt               string           `json:"createdAt"`
}

type ResourceResponse struct {
	URL         string          `validate:"required,max=1000" faker:"url" json:"url,omitempty"`
	Description string          `validate:"max=100" faker:"len=100" json:"description,omitempty"`
	Setting     SettingResponse `validate:"-" json:"setting,omitempty"`
}

// GetMessage get a message by request.
func (m messageService) GetMessage(ctx context.Context, httpRequest MessageGetRequest) (*MessageResponse, error) {
	validate := validator.New()
	err := validate.Struct(httpRequest)
	if err != nil {
		return nil, RequestError
	}

	//userAuth := getUserAuth(ctx)
	message, err := m.param.MessageRepository.FindByCode(httpRequest.MessageId)
	if err != nil {
		return nil, fmt.Errorf("can't get an message from the database: %w", err)
	}

	if message == nil {
		return nil, NotFoundError
	}

	var response MessageResponse
	err = copier.Copy(&response, &message)
	if err != nil {
		return nil, fmt.Errorf("can't copy data for the response: %w", err)
	}
	err = copier.Copy(&response.Resource, &message.Resource)
	if err != nil {
		return nil, fmt.Errorf("can't copy resource data for the response: %w", err)
	}
	err = copier.Copy(&response.Resource.Setting, &message.Resource.Setting)
	if err != nil {
		return nil, fmt.Errorf("can't copy setting data for the response: %w", err)
	}
	response.SendAt = message.SendAt.Format("2006-01-02 15:04:05")
	response.CreatedAt = message.CreatedAt.Format("2006-01-02 15:04:05")

	return &response, nil
}

// getTimeFromStr returns date time in the time format.
func getTimeFromStr(dateTime string) (time.Time, error) {
	layout := "2006-01-02 15:04:05"
	parse, err := time.Parse(layout, dateTime)
	if err != nil {
		return parse, err
	}

	return parse, nil
}

func getUserAuth(ctx context.Context) *auth.UserAuth {
	return ctx.Value("userAuth").(*auth.UserAuth)
}
