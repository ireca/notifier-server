package services

import (
	"fmt"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	sInterfaces "gitlab.com/ireca/notifier-server/internal/services/interfaces"
)

type resourceService struct {
	rRep interfaces.ResourceRepository
}

func NewResourceService(rRep interfaces.ResourceRepository) sInterfaces.ResourceServiceInterface {
	return resourceService{rRep}
}

func (r resourceService) CreateIfNotExist(model models.Resource) (int64, error) {
	resourceCode, err := getHash(model.URL)
	if err != nil {
		return 0, fmt.Errorf("can't generate hash by resource url: %w", err)
	}
	resource, err := r.rRep.FindByCode(resourceCode)
	if err != nil {
		return 0, fmt.Errorf("can't get resource from the database: %w", err)
	}
	if resource != nil {
		return resource.ID, nil
	}

	resourceId, err := r.rRep.Save(models.Resource{
		UserId:      model.UserId,
		Code:        resourceCode,
		URL:         model.URL,
		Description: model.Description,
	})
	if err != nil {
		return 0, fmt.Errorf("can't create resource in the database: %w", err)
	}

	return resourceId, err
}
