package interfaces

import (
	"context"
	"gitlab.com/ireca/notifier-server/internal/models"
)

type MessageSenderInterface interface {
	// SendMessage Save create journal record.
	SendMessage(ctx context.Context, message models.Message) (isSent bool, status int, content string, err error)
}
