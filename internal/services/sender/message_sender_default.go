package sender

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/services/sender/interfaces"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"html/template"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

var OAuthTokenExpiredError = fmt.Errorf("oauth token has been expired")

type messageSenderDefaultService struct {
	cfg config.Config
}

func NewMessageSenderDefaultService(cfg config.Config) interfaces.MessageSenderInterface {
	return &messageSenderDefaultService{cfg}
}

// SendMessage send a message.
func (s messageSenderDefaultService) SendMessage(ctx context.Context, message models.Message) (isSent bool, status int, content string, err error) {
	isSent = false
	status, content, err = s.sendRequest(ctx, message)
	if err != nil {
		utils.LogError("can't send message to the resource: %s", err.Error())
	}
	if status == message.SuccessHttpStatus {
		isSent = true
	}
	if message.Resource.Setting.AuthType == models.OAuth2Type &&
		(utils.InArray(message.Resource.Setting.Auth.CheckTokenHTTPCodes, status) ||
			strings.Contains(content, message.Resource.Setting.Auth.CheckTokenSubResponse)) {
		log.Println("message_token_expired", status, content)

		return false, 0, "", OAuthTokenExpiredError
	}

	return isSent, status, content, nil
}

// sendRequest send a request from a message.
func (s messageSenderDefaultService) sendRequest(ctx context.Context, message models.Message) (status int, content string, err error) {
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Timeout: time.Duration(s.cfg.Sender.Timeout) * time.Second,
	}

	url, err := s.getModifiedURL(message.Resource.URL, message.URLParams)
	if err != nil {
		return 0, "", fmt.Errorf("can't modify URL:%v", err)
	}

	req, err := http.NewRequest(
		strings.ToUpper(message.Command),
		url,
		strings.NewReader(message.Content),
	)
	if err != nil {
		return 0, "", err
	}
	req.WithContext(ctx)
	const headerKeyIndex = 0
	const headerValueIndex = 1
	for _, header := range message.Headers {
		hParts := strings.Split(header, ":")
		if len(hParts) < 2 {
			utils.LogError("wrong header: %s", header)
			continue
		}
		if hParts[headerKeyIndex] == "Authorization" {
			hParts[headerValueIndex] =
				strings.Replace(hParts[headerValueIndex], "{{.token}}", message.Resource.Setting.Auth.Token, 1)
		}

		req.Header.Add(hParts[headerKeyIndex], hParts[headerValueIndex])
	}

	log.Println("send_message_request:", message.Content)

	resp, err := client.Do(req)
	if err != nil {
		return 0, "", err
	}
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}
	defer utils.ResourceClose(resp.Body)

	return resp.StatusCode, string(respBody), nil
}

func (s messageSenderDefaultService) getModifiedURL(URL string, params []string) (string, error) {
	if len(params) == 0 {
		return URL, nil
	}

	t, err := template.New("modified URL").Parse(URL)
	if err != nil {
		return "", fmt.Errorf("can't parse URL: %v", err)
	}

	tParams, err := s.getConvertedParams(params)
	if err != nil {
		return "", fmt.Errorf("can't execute template: %v", err)
	}

	var tempBuff bytes.Buffer
	tempBuffWriter := bufio.NewWriter(&tempBuff)
	err = t.Execute(tempBuffWriter, tParams)
	if err != nil {
		return "", fmt.Errorf("can't execute template: %v", err)
	}

	err = tempBuffWriter.Flush()
	if err != nil {
		return "", fmt.Errorf("flush error: %v", err)
	}

	return tempBuff.String(), nil
}

func (s messageSenderDefaultService) getConvertedParams(params []string) (map[string]string, error) {
	const key = 0
	const value = 1
	tempParams := make(map[string]string)
	for _, param := range params {
		hParts := strings.Split(param, ":")
		if len(hParts) < 2 {
			return nil, fmt.Errorf("wrong urlParams: %d", len(hParts))
		}
		tempParams[hParts[key]] = hParts[value]
	}

	return tempParams, nil
}
