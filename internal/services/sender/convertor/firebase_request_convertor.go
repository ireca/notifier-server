package convertor

import (
	"encoding/json"
	"errors"
	"firebase.google.com/go/messaging"
	"fmt"
	"gitlab.com/ireca/notifier-server/config"
	"strings"
)

type FirebaseRequestConvertorInterface interface {
	GetFirebaseMessage(jsnContent string) (*messaging.Message, error)
}

type firebaseRequestConvertor struct {
	cfg config.Config
}

var WrongFCMFormat = fmt.Errorf("wrong FCM format")
var WrongV1Format = fmt.Errorf("wrong V1 format")

func NewFirebaseRequestConvertor() FirebaseRequestConvertorInterface {
	return &firebaseRequestConvertor{}
}

type NotificationRequest struct {
	Title string `json:"title"`
	Body  string `json:"body"`
	Sound string `json:"sound"`
	Badge int    `json:"badge"`
}

type DataFCMRequest struct {
	Title   string `json:"title"`
	Message string `json:"message"`
}

type RequestFCM struct {
	To           string              `json:"to"`
	Notification NotificationRequest `json:"notification"`
	Data         map[string]string   `json:"data"`
	Priority     string              `json:"priority"`
	Sound        string              `json:"sound"`
}

func (f firebaseRequestConvertor) GetFirebaseMessage(jsnContent string) (*messaging.Message, error) {
	m, err := f.getMessageByReqFCM(jsnContent)
	if err != nil {
		if errors.Is(err, WrongFCMFormat) {
			return f.getMessageByRegV1(jsnContent)
		} else {
			return nil, err
		}
	}

	return m, err
}

func (f firebaseRequestConvertor) getMessageByReqFCM(cnt string) (*messaging.Message, error) {
	req := RequestFCM{}
	req.Data = make(map[string]string)
	err := json.Unmarshal([]byte(cnt), &req)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling FCM request: %w", err)
	}

	if req.Data["title"] == "" || req.Data["message"] == "" {
		return nil, WrongFCMFormat
	}

	token := ""
	topic := ""
	if strings.Contains(req.To, "/topics/") {
		topic = strings.Replace(req.To, "/topics/", "", 1)
	} else {
		token = req.To
	}

	androidCfg := messaging.AndroidConfig{}
	if req.Priority != "" {
		androidCfg.Priority = req.Priority
	}

	an := messaging.AndroidNotification{}
	an.Priority = messaging.PriorityHigh
	if req.Notification.Sound != "" {
		an.Sound = req.Notification.Sound
		androidCfg.Notification = &an
	}

	apns := messaging.APNSConfig{
		Headers: map[string]string{
			"apns-push-type": "alert",
			"apns-priority":  "10",
		},
		Payload: &messaging.APNSPayload{
			Aps: &messaging.Aps{
				Alert: &messaging.ApsAlert{
					Title: req.Data["title"],
					Body:  req.Data["body"],
				},
				Badge: &req.Notification.Badge,
				Sound: "default",
			},
		},
	}

	return &messaging.Message{
		Notification: &messaging.Notification{
			Title: req.Data["title"],
			Body:  req.Data["message"],
		},
		Android: &androidCfg,
		APNS:    &apns,
		Data:    req.Data,
		Topic:   topic,
		Token:   token,
	}, nil
}

type MessageV1Request struct {
	Token        string              `json:"token"`
	Topic        string              `json:"topic"`
	Notification NotificationRequest `json:"notification"`
	Priority     string              `json:"priority"`
	Sound        string              `json:"sound"`
	Data         map[string]string   `json:"data"`
}

type RequestV1 struct {
	Message MessageV1Request `json:"message"`
}

func (f firebaseRequestConvertor) getMessageByRegV1(cnt string) (*messaging.Message, error) {
	reqV1 := RequestV1{}
	err := json.Unmarshal([]byte(cnt), &reqV1)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling V1 request: %w", err)
	}

	if reqV1.Message.Notification.Title != "" || reqV1.Message.Notification.Body != "" {
		return nil, WrongV1Format
	}

	return &messaging.Message{
		Notification: &messaging.Notification{
			Title: reqV1.Message.Notification.Title,
			Body:  reqV1.Message.Notification.Body,
		},
		Topic: reqV1.Message.Topic,
		Token: reqV1.Message.Token,
	}, nil
}
