package sender

import (
	"context"
	_ "firebase.google.com/go"
	firebase "firebase.google.com/go"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/services/sender/convertor"
	"gitlab.com/ireca/notifier-server/internal/services/sender/interfaces"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"google.golang.org/api/option"
	_ "google.golang.org/api/option"
	"log"
	"os"
	"strings"
)

type messageSenderFirebaseService struct {
	cfg config.Config
	cvr convertor.FirebaseRequestConvertorInterface
}

func NewMessageSenderFirebaseService(cfg config.Config, cvr convertor.FirebaseRequestConvertorInterface) interfaces.MessageSenderInterface {
	return &messageSenderFirebaseService{cfg, cvr}
}

// SendMessage send a message.
func (s messageSenderFirebaseService) SendMessage(ctx context.Context, message models.Message) (isSent bool, status int, content string, err error) {
	log.Println("send_instantly_message_request:", message.Content)

	crlFilePath, err := s.getCredentialFilePath(message.Resource.URL)
	if err != nil {
		return false, 500, "", err
	}

	if !utils.IsValidJSON(message.Content) {
		return false, 500, "", fmt.Errorf("invalid json content")
	}

	if _, err = os.Stat(crlFilePath); err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return false, 500, "", fmt.Errorf("credential file doesn't exist to path: %s", crlFilePath)
		} else if errors.Is(err, os.ErrPermission) {
			return false, 500, "", fmt.Errorf("credential file doesn't have permission to path: %s", crlFilePath)
		} else {
			return false, 500, "", fmt.Errorf("credential file couldn't read: %s, %v", crlFilePath, err)
		}
	}

	opt := option.WithCredentialsFile(crlFilePath)
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		return false, 500, "", fmt.Errorf("error initializing app: %v", err)
	}

	clientM, err := app.Messaging(ctx)
	if err != nil {
		return false, 500, "", fmt.Errorf("error getting Firebase Messaging client: %v", err)
	}

	firebaseMsg, err := s.cvr.GetFirebaseMessage(message.Content)
	if err != nil {
		return false, 500, "", fmt.Errorf("can't convert content to firebase message: %v", err)
	}

	response, err := clientM.Send(ctx, firebaseMsg)
	if err != nil {
		return false, 500, "", fmt.Errorf("can't send message to the resource: %v", err)
	}

	log.Println("send_instantly_message_firebase_response:", response)

	return true, 200, response, nil
}

func (s messageSenderFirebaseService) getCredentialFilePath(toUrl string) (string, error) {
	for _, app := range s.cfg.Resource.FireBase.Applications {
		if strings.Contains(toUrl, app.ProjectId) {
			return app.CrlFilePath, nil
		}
	}

	return "", fmt.Errorf("can't find credential file for URL: %s", toUrl)
}
