package factory

import (
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/services/sender"
	"gitlab.com/ireca/notifier-server/internal/services/sender/convertor"
	"gitlab.com/ireca/notifier-server/internal/services/sender/interfaces"
	"strings"
)

func NewMessageSenderService(cfg config.Config, url string) interfaces.MessageSenderInterface {
	if isFirebaseMess(cfg.Resource.FireBase, url) {
		return sender.NewMessageSenderFirebaseService(cfg, convertor.NewFirebaseRequestConvertor())
	}

	return sender.NewMessageSenderDefaultService(cfg)
}

func isFirebaseMess(cfg config.FireBase, toUrl string) bool {
	for _, uri := range cfg.URLs {
		if strings.Contains(toUrl, uri) {
			return true
		}
	}

	return false
}
