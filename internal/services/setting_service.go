package services

import (
	"context"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/jinzhu/copier"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/services/auth"
)

type SettingCreateRequest struct {
	SettingId           string                   `validate:"required,max=64" faker:"uuid_hyphenated" json:"settingId,omitempty"`
	Title               string                   `validate:"max=100" faker:"len=100" json:"title,omitempty"`
	CallbackURL         string                   `validate:"max=1000" faker:"url" json:"callbackUrl,omitempty"`
	Count               int                      `validate:"-" faker:"boundary_start=1, boundary_end=10" json:"count,omitempty"`
	Intervals           []int                    `validate:"-" faker:"boundary_start=1, boundary_end=10" json:"intervals,omitempty"`
	Timeout             int                      `validate:"-" faker:"boundary_start=1, boundary_end=10" json:"timeout,omitempty"`
	SuccessHTTPStatuses []int                    `validate:"-" faker:"boundary_start=1, boundary_end=600" json:"successHttpStatuses,omitempty"`
	Description         string                   `validate:"max=100" faker:"len=100" json:"description,omitempty"`
	AuthType            string                   `validate:"required,oneof='oauth2' 'sdk' 'none'" faker:"oneof: oauth2" json:"authType,omitempty"`
	Auth                AuthSettingCreateRequest `validate:"-" json:"auth,omitempty"`
}

type AuthSettingCreateRequest struct {
	Token                 string   `validate:"max=8" faker:"len=1200" json:"token,omitempty"`
	URL                   string   `validate:"max=10" faker:"len=1200" json:"url,omitempty"`
	Headers               []string `validate:"-" faker:"slice_len=3, len=10" json:"headers,omitempty"`
	Body                  string   `validate:"max=300" faker:"len=300" json:"body,omitempty"`
	CheckTokenHTTPCodes   []int    `validate:"-" faker:"boundary_start=0, boundary_end=600" json:"checkTokenHTTPCodes,omitempty"`
	CheckTokenSubResponse string   `validate:"max=300" faker:"len=300" json:"checkTokenSubResponse,omitempty"`
	Description           string   `validate:"max=100" faker:"len=100" json:"description,omitempty"`
}

type SettingRouteParameters struct {
	Config            config.Config
	SettingRepository interfaces.SettingRepository
}

// CreateSetting create a setting by request.
func CreateSetting(ctx context.Context, request SettingCreateRequest, param SettingRouteParameters) (error error) {
	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		return RequestError
	}

	userAuth := ctx.Value("userAuth").(*auth.UserAuth)
	isExist, err := param.SettingRepository.IsInDatabase(ctx, userAuth.User.ID, request.SettingId)
	if err != nil {
		return fmt.Errorf("can't get settings from the database: %w", err)
	}
	if isExist {
		return fmt.Errorf("%w settingId=%s", AlreadyExistError, request.SettingId)
	}

	var setting models.Setting
	err = copier.Copy(&setting, &request)
	if err != nil {
		return fmt.Errorf("can't copy data from the request: %w", err)
	}
	setting.UserId = userAuth.User.ID
	err = param.SettingRepository.Save(ctx, setting)
	if err != nil {
		return fmt.Errorf("can't create an setting in the database: %w", err)
	}

	return nil
}

// UpdateSetting update a setting by request.
func UpdateSetting(ctx context.Context, request SettingCreateRequest, param SettingRouteParameters) (error error) {
	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		return RequestError
	}

	userAuth := ctx.Value("userAuth").(*auth.UserAuth)
	model, err := param.SettingRepository.FindByCode(ctx, userAuth.User.ID, request.SettingId)
	if err != nil {
		return fmt.Errorf("can't get setting from the database: %w", err)
	}
	if model == nil {
		return fmt.Errorf("%w, settingId=%s", NotFoundError, request.SettingId)
	}

	var setting models.Setting
	err = copier.Copy(&setting, &request)
	if err != nil {
		return fmt.Errorf("can't copy data from the request: %w", err)
	}
	setting.UserId = userAuth.User.ID
	setting.Auth.SettingId = setting.ID
	err = param.SettingRepository.Update(ctx, setting)
	if err != nil {
		return fmt.Errorf("can't update an setting in the database: %w", err)
	}

	return nil
}

type SettingDeleteRequest struct {
	SettingId string `validate:"required,max=64"`
}

// DeleteSetting delete an setting by request.
func DeleteSetting(ctx context.Context, settingDeleteRequest SettingDeleteRequest, param SettingRouteParameters) error {
	validate := validator.New()
	err := validate.Struct(settingDeleteRequest)
	if err != nil {
		return RequestError
	}

	userAuth := ctx.Value("userAuth").(*auth.UserAuth)
	isExist, err := param.SettingRepository.IsInDatabase(ctx, userAuth.User.ID, settingDeleteRequest.SettingId)
	if err != nil {
		return fmt.Errorf("can't get setting from the database: %w", err)
	}
	if !isExist {
		return fmt.Errorf("%w, this setting isn't exist, userId=%s", NotFoundError, settingDeleteRequest.SettingId)
	}

	err = param.SettingRepository.Delete(ctx, userAuth.User.ID, settingDeleteRequest.SettingId)
	if err != nil {
		return fmt.Errorf("can't delete an setting from the database: %w", err)
	}

	return nil
}

type SettingGetRequest struct {
	SettingId string `validate:"required,min=1,max=64"`
}

type SettingResponse struct {
	SettingId   string `json:"settingId,omitempty" copier:"Code"`
	Title       string `json:"title,omitempty" copier:"Title"`
	CallbackURL string `json:"callbackURL,omitempty" copier:"CallbackURL"`
	Count       int    `json:"count,omitempty" copier:"Count"`
	Intervals   []int  `json:"intervals,omitempty" copier:"Intervals"`
	Timeout     int    `json:"timeout,omitempty" copier:"Timeout"`
	Description string `json:"description,omitempty" copier:"Description"`
}

// GetSetting get an setting by request.
func GetSetting(ctx context.Context, httpRequest SettingGetRequest, param SettingRouteParameters) (*SettingResponse, error) {
	validate := validator.New()
	err := validate.Struct(httpRequest)
	if err != nil {
		return nil, RequestError
	}

	userAuth := ctx.Value("userAuth").(*auth.UserAuth)
	setting, err := param.SettingRepository.FindByCode(ctx, userAuth.User.ID, httpRequest.SettingId)
	if err != nil {
		return nil, fmt.Errorf("can't get an setting from the database: %w", err)
	}

	if setting == nil {
		return nil, NotFoundError
	}

	var response SettingResponse
	err = copier.Copy(&response, &setting)
	if err != nil {
		return nil, fmt.Errorf("can't copy data for the response: %w", err)
	}

	return &response, nil
}

type SettingsGetRequest struct {
	Limit  int `validate:"numeric"`
	Offset int `validate:"numeric"`
}

// GetSettings get users by request.
func GetSettings(ctx context.Context, httpRequest SettingsGetRequest, param SettingRouteParameters) ([]SettingResponse, error) {
	validate := validator.New()
	err := validate.Struct(httpRequest)
	if err != nil {
		return nil, RequestError
	}

	userAuth := ctx.Value("userAuth").(*auth.UserAuth)
	users, err := param.SettingRepository.FindAll(ctx, userAuth.User.ID, httpRequest.Limit, httpRequest.Offset)
	if err != nil {
		return nil, fmt.Errorf("can't find users in the database: %w", err)
	}

	responses, err := getSettingsResponses(users)
	if err != nil {
		return nil, fmt.Errorf("can't get responses: %w", err)
	}

	return responses, nil
}

// getSettingsResponses gets setting responses.
func getSettingsResponses(users *map[int]models.Setting) ([]SettingResponse, error) {
	var responses []SettingResponse
	for _, setting := range *users {
		var response SettingResponse
		err := copier.Copy(&response, &setting)
		if err != nil {
			return responses, fmt.Errorf("can't copy data for the response: %w", err)
		}
		responses = append(responses, response)
	}

	return responses, nil
}
