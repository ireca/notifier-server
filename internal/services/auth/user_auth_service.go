package auth

import (
	"fmt"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"net/http"
	"strings"
)

const (
	Admin = "admin"
	User  = "user"
)

const defaultAdminToken = "54d1ba805e2a4891aeac9299b618945e"

type UserAuth struct {
	Role string
	User models.User
}

type TokenProvider interface {
	FindByToken(code string) (*models.User, error)
	Update(model models.User) error
}

type UserAuthService struct {
	tokenProvider TokenProvider
	config        config.Config
}

func NewUserAuth(userRepository interfaces.UserRepository, config config.Config) *UserAuthService {
	return &UserAuthService{userRepository, config}
}

func (u UserAuthService) GetAuthUser(token string) (*UserAuth, error) {
	if len(token) < 32 {
		return nil, fmt.Errorf("user doesn't have correct a token")
	}

	user, err := u.tokenProvider.FindByToken(token)
	if err != nil {
		return nil, err
	}
	if user == nil {
		return nil, fmt.Errorf("user not found")
	}

	return &UserAuth{Role: user.Role, User: *user}, nil
}

func (u UserAuthService) GetToken(r *http.Request) string {
	token := r.Header.Get("Authorization")
	token = strings.Replace(token, "Bearer", "", 1)

	return strings.ReplaceAll(token, " ", "")
}

func (u UserAuthService) UpdateDefaultAdminTokenByCfg(cfg config.Auth) (bool, error) {
	if len(cfg.AdminAuthToken) < 32 {
		return false, fmt.Errorf("admin auth token less then 32 chars in your config")
	}

	user, err := u.tokenProvider.FindByToken(defaultAdminToken)
	if err != nil {
		return false, fmt.Errorf("can't find user by default admin token: %v", err)
	}

	if user == nil {
		return false, nil
	}
	user.AuthToken = cfg.AdminAuthToken

	return true, u.tokenProvider.Update(*user)
}
