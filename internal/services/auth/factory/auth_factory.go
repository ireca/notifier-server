package factory

import (
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/services/auth"
)

func NewUserAuthService(userRepository interfaces.UserRepository, config config.Config) *auth.UserAuthService {
	return auth.NewUserAuth(userRepository, config)
}
