package interfaces

import (
	"gitlab.com/ireca/notifier-server/internal/services/auth"
	"net/http"
)

type UserAuthHandler interface {
	GetAuthUser(token string) (auth.UserAuth, error)
	GetToken(r *http.Request) string
}
