package services

import (
	"context"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/services/auth"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"log"
)

type QInterface interface {
	Connection() (*amqp.Connection, *amqp.Channel, error)
	CreateQueue(ch *amqp.Channel) error
	ConsumerRun(ctx context.Context, ch *amqp.Channel) error
	Disconnect(ch *amqp.Channel) error
}

type qService struct {
	cfg         config.Queue
	messService MessServiceInterface
	ua          *auth.UserAuthService
	jRep        interfaces.JournalRepository
}

func NewQueueService(cfg config.Queue, messService MessServiceInterface, ua *auth.UserAuthService, jRep interfaces.JournalRepository) QInterface {
	return qService{cfg, messService, ua, jRep}
}

func (q qService) Connection() (*amqp.Connection, *amqp.Channel, error) {
	conn, err := amqp.Dial(q.cfg.URL)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to open connect to RabbitMQ queue: %v", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		return nil, nil, fmt.Errorf("failed to open a channel: %v", err)
	}

	err = ch.Qos(100, 0, false)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to set QOS for a channel: %v", err)
	}

	return conn, ch, err
}

func (q qService) CreateQueue(ch *amqp.Channel) error {
	queue, err := ch.QueueDeclare(
		q.cfg.Name,
		q.cfg.Durable,
		q.cfg.AutoDelete,
		q.cfg.Exclusive,
		q.cfg.NoWait,
		q.cfg.Args,
	)
	if err != nil {
		return fmt.Errorf("failed to declare a queue: %v", err)
	}

	err = ch.ExchangeDeclare(
		q.cfg.Exchange.Name,
		q.cfg.Exchange.Kind,
		q.cfg.Exchange.Durable,
		q.cfg.Exchange.AutoDelete,
		q.cfg.Exchange.Internal,
		q.cfg.Exchange.NoWait,
		q.cfg.Exchange.Args,
	)
	if err != nil {
		return fmt.Errorf("failed to declare an exchange: %v", err)
	}

	err = ch.QueueBind(
		queue.Name,
		q.cfg.Bind.Key,
		q.cfg.Exchange.Name,
		q.cfg.Bind.NoWait,
		q.cfg.Bind.Args,
	)
	if err != nil {
		return fmt.Errorf("failed to bind an queue to exchange: %v", err)
	}

	return nil
}

func (q qService) ConsumerRun(ctx context.Context, ch *amqp.Channel) error {
	chMess, err := ch.Consume(
		q.cfg.Consumer.Name,
		q.cfg.Consumer.Consumer,
		q.cfg.Consumer.AutoAck,
		q.cfg.Consumer.Exclusive,
		q.cfg.Consumer.NoLocal,
		q.cfg.Consumer.NoWait,
		q.cfg.Consumer.Args,
	)
	if err != nil {
		return fmt.Errorf("failed to register a consumer: %v", err)
	}

	go func() {
		for message := range chMess {
			q.ask(message)
			var requests []MessageCreateRequest
			err = json.Unmarshal(message.Body, &requests)
			if err != nil {
				utils.LogError("i can't decode message from queue: %v", err)
			} else {
				log.Printf("received a message: %s", message.Body)

				if _, ok := message.Headers["Authorization"]; !ok {
					utils.LogError("message not contain auth header")
					continue
				}

				userAuth, errUa := q.ua.GetAuthUser(message.Headers["Authorization"].(string))
				if errUa != nil {
					utils.LogError("i can't auth user by token: %v", errUa)
					continue
				}
				uaCtx := context.WithValue(ctx, "userAuth", userAuth)
				_, err = q.messService.CreateMessages(uaCtx, requests)
				if err != nil {
					utils.LogError("can't create a message %v", err)
					continue
				}
			}
		}
	}()

	return nil
}

func (q qService) ask(mess amqp.Delivery) {
	err := mess.Ack(false)
	if err != nil {
		utils.LogError("can't ask a message %v", err)
	}
}

func (q qService) Disconnect(ch *amqp.Channel) error {
	err := ch.Close()
	if err != nil {
		fmt.Printf("can't close queue channel: %v", err)
	}

	return nil
}
