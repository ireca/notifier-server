package interfaces

type MessageCallbackService interface {
	SendCallbacks()
}
