package interfaces

import "gitlab.com/ireca/notifier-server/config"

type SendMessageService interface {
	Run(senderCfg config.Sender)
}
