package interfaces

import "gitlab.com/ireca/notifier-server/internal/models"

type ResourceServiceInterface interface {
	CreateIfNotExist(model models.Resource) (int64, error)
}
