package interfaces

import "gitlab.com/ireca/notifier-server/internal/models"
import "context"

type AuthTokenServiceInterface interface {
	UpdateToken(ctx context.Context, userId int, aSetting models.AuthTokenSetting) error
}
