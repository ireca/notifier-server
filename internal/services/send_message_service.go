package services

import (
	"context"
	"errors"
	"fmt"
	"github.com/getsentry/sentry-go"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	sInterfaces "gitlab.com/ireca/notifier-server/internal/services/interfaces"
	"gitlab.com/ireca/notifier-server/internal/services/sender"
	"gitlab.com/ireca/notifier-server/internal/services/sender/factory"
	"log"
	"sync"
	"time"
)

type sendMessageService struct {
	cfg  config.Config
	rep  interfaces.MessageRepository
	jRep interfaces.JournalRepository
	ats  sInterfaces.AuthTokenServiceInterface
}

func NewMessageSenderService(cfg config.Config, rep interfaces.MessageRepository, jRep interfaces.JournalRepository,
	ats sInterfaces.AuthTokenServiceInterface) sInterfaces.SendMessageService {
	return &sendMessageService{cfg, rep, jRep, ats}
}

type updateToken struct {
	UserId int
	Auth   models.AuthTokenSetting
}

// Run cycle for send messages.
// TODO: The interval option of an message doesn't use.
func (s sendMessageService) Run(senderCfg config.Sender) {
	ctx := context.Background()
	for {
		wg := &sync.WaitGroup{}
		tCtx, cancel := context.WithTimeout(ctx, time.Duration(senderCfg.Timeout*senderCfg.ItemCount)*time.Second)
		//TODO: you should load messages for every user otherwise update token won't work.
		messages, err := s.rep.FindAll(senderCfg.MaxAttempts, senderCfg.ItemCount, 0)
		if err != nil {
			fmt.Printf("can't get messages: " + err.Error())
		}
		defer cancel()

		ut := updateToken{0, models.AuthTokenSetting{}}
		for _, message := range *messages {
			m := message
			ss := factory.NewMessageSenderService(s.cfg, m.Resource.URL)
			wg.Add(1)
			go func() {
				defer func() {
					if errR := recover(); errR != nil {
						sentry.CurrentHub().Recover(errR)
						sentry.Logger.Printf("panic recovered: %v", errR)
						sentry.Flush(time.Second * 5)
						panic(errR)
					}
					wg.Done()
				}()

				isSent, status, content, errM := ss.SendMessage(tCtx, m)
				if errM != nil {
					if errors.Is(errM, sender.OAuthTokenExpiredError) {
						cancel()
						log.Println("you should update token!")
						ut = updateToken{m.UserId, m.Resource.Setting.Auth}
						return
					} else {
						log.Println("service couldn't send message:", errM)
					}
				}

				if isSent {
					err = s.rep.MarkSent(message.Code)
					if err != nil {
						log.Printf("can't mark a message as sent: %v", err)
					}
					log.Println("the message was sent!", message.Code)
				} else {
					err = s.rep.MarkUnSent(message.Code, message.AttemptCount+1)
					if err != nil {
						log.Printf("can't mark a message as not sent: %v", err)
					}
					log.Println("the message wasn't sent:", message.Code)
				}

				if (senderCfg.Journal.IsWriteNotSentRequest && !isSent) ||
					(senderCfg.Journal.IsWriteSentRequest && isSent) {
					journal := models.Journal{
						UserId:          message.User.ID,
						ResourceId:      message.ResourceId,
						MessageId:       message.ID,
						ResponseStatus:  status,
						ResponseContent: content,
					}
					err = s.jRep.Save(journal)
					if err != nil {
						log.Printf("can't write a record to the journal: %v", err)
					}
				}

				if (senderCfg.IsDeleteNotSentMessages && (message.AttemptCount == senderCfg.MaxAttempts)) ||
					(senderCfg.IsDeleteSentMessages && isSent) {
					err = s.rep.Delete(message.Code)
					if err != nil {
						log.Printf("can't delete a message: %v", err)
					}
				}
				return
			}()
		}

		wg.Wait()
		if ut.UserId != 0 {
			log.Println("update token!")
			err = s.ats.UpdateToken(ctx, ut.UserId, ut.Auth)
			if err != nil {
				log.Printf("can't update token:%v", err)
			}
		} else if len(*messages) == 0 {
			time.Sleep(time.Duration(senderCfg.LoadInterval) * time.Second)
		}
	}

}
