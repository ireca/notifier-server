package models

import "time"

const (
	OAuth2Type   = "oauth2"
	NoneAuthType = "none"
)

type AuthTokenSetting struct {
	ID                    int       `copier:"-"` // External resource ID.
	SettingId             int       `copier:"-"` // Setting ID.
	Token                 string    `copier:"Token"`
	URL                   string    `copier:"URL"`
	Headers               []string  `copier:"Headers"`
	Body                  string    `copier:"Body"`
	CheckTokenHTTPCodes   []int     `copier:"CheckTokenHTTPCodes"`
	CheckTokenSubResponse string    `copier:"CheckTokenSubResponse"`
	Description           string    `copier:"Description"`
	CreatedAt             time.Time // Date and time the message was created.
	UpdatedAt             time.Time // Date and time the message was updated.
}
