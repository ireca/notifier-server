package server

import (
	"compress/flate"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/repositories/interfaces"
	"gitlab.com/ireca/notifier-server/internal/services"
	"gitlab.com/ireca/notifier-server/internal/services/auth"
	"golang.org/x/net/context"
	"net/http"
	"strings"
	"time"
)

type RouteParameters struct {
	Config             config.Config
	UserRepository     interfaces.UserRepository
	SettingRepository  interfaces.SettingRepository
	ResourceRepository interfaces.ResourceRepository
	MessageRepository  interfaces.MessageRepository
	JournalRepository  interfaces.JournalRepository
}

// GetRouters returns all routers for the server.
func GetRouters(uh *auth.UserAuthService, p RouteParameters, instantlyP RouteParameters) *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(time.Duration(p.Config.RequestTimeoutSec) * time.Second))
	compressor := middleware.NewCompressor(flate.DefaultCompression)
	r.Use(compressor.Handler)

	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token := uh.GetToken(r)
			userAuth, err := uh.GetAuthUser(token)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}
			if strings.HasPrefix(r.RequestURI, "/api/v1/users") == true && userAuth.Role != auth.Admin {
				http.Error(w, "access for this route is denied", http.StatusUnauthorized)
				return
			}

			ctx := context.WithValue(r.Context(), "userAuth", userAuth)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	})

	var params = services.UserRouteParameters{
		Config:         p.Config,
		UserRepository: p.UserRepository,
	}
	r = getCreateUserRoute(r, params)
	r = getUpdateUserRoute(r, params)
	r = getDeleteUserRoute(r, params)
	r = getUserRoute(r, params)
	r = getUsersRoute(r, params)

	var settingParams = services.SettingRouteParameters{
		Config:            p.Config,
		SettingRepository: p.SettingRepository,
	}
	r = getCreateSettingRoute(r, settingParams)
	r = getUpdateSettingRoute(r, settingParams)
	r = getDeleteSettingRoute(r, settingParams)
	r = getSettingRoute(r, settingParams)
	r = getSettingsRoute(r, settingParams)

	var messParams = services.MessageRouteParameters{
		Config:             p.Config,
		UserRepository:     p.UserRepository,
		ResourceRepository: p.ResourceRepository,
		MessageRepository:  p.MessageRepository,
		SettingRepository:  p.SettingRepository,
	}
	r = getCreateMessagesRoute(r, messParams)
	r = getDeleteMessageRoute(r, messParams)
	r = getMessageRoute(r, messParams)

	var messInstParams = services.MessageRouteParameters{
		Config:             instantlyP.Config,
		UserRepository:     instantlyP.UserRepository,
		ResourceRepository: instantlyP.ResourceRepository,
		MessageRepository:  instantlyP.MessageRepository,
		SettingRepository:  instantlyP.SettingRepository,
	}
	r = getCreateInstantlyMessagesRoute(r, messInstParams)
	r = getDeleteInstantlyMessageRoute(r, messInstParams)
	r = getInstantlyMessageRoute(r, messInstParams)

	r = getHealthCheckerRoute(r)

	return r
}
