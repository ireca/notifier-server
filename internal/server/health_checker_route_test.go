package server

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/repositories/factory"
	factory2 "gitlab.com/ireca/notifier-server/internal/services/auth/factory"
	"log"
	"net/http/httptest"
	"strings"
	"testing"
)

// TestGetHealthCheckerRoute test crud actions for messages.
func TestGetHealthCheckerRoute(t *testing.T) {
	var pool *pgxpool.Pool
	ctx := context.Background()
	configPath, err := GetConfigPath()
	if err != nil {
		log.Fatalf("i can't get path to the configuration file:" + err.Error())
	}
	configFromFile, err := config.LoadConfigFile(configPath)
	if err != nil {
		log.Fatalf("i can't load configuration file:" + err.Error())
	}
	cfg, err := config.GetConfigSettings(configFromFile)
	if err != nil {
		log.Fatalf("Can't read config: %s", err.Error())
	}

	pool, _ = pgxpool.Connect(ctx, cfg.DatabaseDsn)
	defer pool.Close()

	userRepository := factory.NewUserRepository(ctx, pool)
	userAuthService := factory2.NewUserAuthService(userRepository, cfg)
	routeParameters := RouteParameters{}
	instRouteParameters := RouteParameters{}
	r := GetRouters(userAuthService, routeParameters, instRouteParameters)
	ts := httptest.NewServer(r)
	defer ts.Close()

	fmt.Println("start test: health check")
	req, err := getRequest(
		ts.URL+"/api/v1/healthCheck", nil, "GET", strings.NewReader(""), cfg.Auth.AdminAuthToken,
	)
	assert.NoError(t, err)
	resp, _ := sendRequest(t, req)
	require.NoError(t, err)

	assert.Equal(t, 200, resp.StatusCode)
}
