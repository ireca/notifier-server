package server

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/ireca/notifier-server/internal/handlers"
	"net/http"
)

// getHealthCheckerRoute gets health checker route.
func getHealthCheckerRoute(r *chi.Mux) *chi.Mux {
	r.Get("/api/v1/healthCheck", func(w http.ResponseWriter, r *http.Request) {
		handlers.HealthCheckerHandler(r, w)
	})

	return r
}
