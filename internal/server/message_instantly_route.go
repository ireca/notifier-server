package server

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/ireca/notifier-server/internal/handlers"
	"gitlab.com/ireca/notifier-server/internal/services"
	"net/http"
)

// getCreateMessagesRoute gets create messages route.
func getCreateInstantlyMessagesRoute(r *chi.Mux, params services.MessageRouteParameters) *chi.Mux {
	r.Post("/api/v1/instantly/messages", func(w http.ResponseWriter, r *http.Request) {
		handlers.CreateInstantlyMessageHandler(r, w, params)
	})

	return r
}

// getCreateMessagesRoute gets delete message route.
func getDeleteInstantlyMessageRoute(r *chi.Mux, params services.MessageRouteParameters) *chi.Mux {
	r.Delete("/api/v1/instantly/messages", func(w http.ResponseWriter, r *http.Request) {
		handlers.DeletedInstantlyMessageHandler(r, w, params)
	})

	return r
}

// getMessageRoute gets message route.
func getInstantlyMessageRoute(r *chi.Mux, params services.MessageRouteParameters) *chi.Mux {
	r.Get("/api/v1/instantly/message", func(w http.ResponseWriter, r *http.Request) {
		handlers.GetInstantlyMessageHandler(r, w, params)
	})

	return r
}
