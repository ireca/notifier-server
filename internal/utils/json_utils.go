package utils

import "encoding/json"

func IsValidJSON(contentJSON string) bool {
	var js json.RawMessage
	return json.Unmarshal([]byte(contentJSON), &js) == nil
}
