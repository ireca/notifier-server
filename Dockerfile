# specify the base image to  be used for the application, alpine or ubuntu
FROM golang:1.22.5 as builder

ARG BUILD_VERSION=1.0.0
ARG BUILD_COMMIT=d3ada1bf

# create a working directory inside the image.
WORKDIR /app
# copy Go modules and dependencies to image.
COPY . .
# compile application.
RUN GO111MODULE=on GOOS=linux CGO_ENABLED=0 \
    go build -ldflags "-s -w -X main.buildVersion=${BUILD_VERSION} -X main.buildDate=`date -u +%Y-%m-%d.%H:%M:%S` -X main.buildCommit=${BUILD_COMMIT}" \
    -o /out cmd/notifier/main.go
# specify the base image to  be used for the application, alpine or ubuntu.
FROM alpine:latest
RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
        ca-certificates \
    && update-ca-certificates 2>/dev/null || true
RUN apk --no-cache add ca-certificates
# create a working directory inside the image.
WORKDIR /app
# copy Go application to image.
COPY --from=builder /out /bin/cmd

ENTRYPOINT ["/bin/cmd"]