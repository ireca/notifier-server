package main

import (
	"context"
	"fmt"
	"github.com/getsentry/sentry-go"
	"github.com/jackc/pgx/v4/pgxpool"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/ireca/notifier-server/config"
	"gitlab.com/ireca/notifier-server/internal/models"
	"gitlab.com/ireca/notifier-server/internal/repositories/factory"
	"gitlab.com/ireca/notifier-server/internal/server"
	grpc3 "gitlab.com/ireca/notifier-server/internal/server/grpc"
	mpb "gitlab.com/ireca/notifier-server/internal/server/grpc/message_proto"
	upb "gitlab.com/ireca/notifier-server/internal/server/grpc/user_proto"
	"gitlab.com/ireca/notifier-server/internal/services"
	"gitlab.com/ireca/notifier-server/internal/services/auth"
	factory2 "gitlab.com/ireca/notifier-server/internal/services/auth/factory"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"google.golang.org/grpc"
	"gopkg.in/Graylog2/go-gelf.v1/gelf"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	buildVersion string
	buildDate    string
	buildCommit  string
)

type memoryParameters struct {
	Messages  map[string]models.Message
	Resources map[string]models.Resource
}

var queueCh *amqp.Channel

func main() {
	fmt.Printf("Build version:%s\n", buildVersion)
	fmt.Printf("Build date:%s\n", buildDate)
	fmt.Printf("Build commit:%s\n", buildCommit)

	configFromFile, err := config.LoadConfigFile("config.json")
	if err != nil {
		log.Fatalf("i can't load configuration file: %v", err)
	}
	cfg, err := config.GetConfigSettings(configFromFile)
	if err != nil {
		log.Fatalf("Can't read config: %v", err)
	}

	if cfg.Sentry.IsRun {
		err = sentry.Init(sentry.ClientOptions{
			Dsn:                cfg.Sentry.Dsn,
			Debug:              cfg.Sentry.Debug,
			AttachStacktrace:   cfg.Sentry.AttachStacktrace,
			SampleRate:         cfg.Sentry.SampleRate,
			EnableTracing:      cfg.Sentry.EnableTracing,
			TracesSampleRate:   cfg.Sentry.TracesSampleRate,
			IgnoreErrors:       cfg.Sentry.IgnoreErrors,
			IgnoreTransactions: cfg.Sentry.IgnoreTransactions,
			SendDefaultPII:     cfg.Sentry.SendDefaultPII,
			ServerName:         cfg.Sentry.ServerName,
			Release:            cfg.Sentry.Release,
			Dist:               cfg.Sentry.Dist,
			Environment:        cfg.Sentry.Environment,
			MaxBreadcrumbs:     cfg.Sentry.MaxBreadcrumbs,
			MaxSpans:           cfg.Sentry.MaxSpans,
			MaxErrorDepth:      cfg.Sentry.MaxErrorDepth,
		})
		if err != nil {
			log.Fatalf("can't connect to the Sentry service: %v", err)
		}
		defer func() {
			if errR := recover(); errR != nil {
				sentry.CurrentHub().Recover(errR)
				sentry.Logger.Printf("panic recovered: %v", errR)
				sentry.Flush(time.Second * 5)
				panic(errR)
			} else {
				sentry.Flush(time.Second * 5)
			}
		}()

		log.Println("successful connect to the Sentry!")
	}

	var pool *pgxpool.Pool
	ctx := context.Background()
	pool, err = pgxpool.Connect(ctx, cfg.DatabaseDsn)
	if err != nil {
		utils.LogFatalError("Can't connect to the database server: %s", err)
	} else {
		psqlVer := ""
		_ = pool.QueryRow(ctx, "SELECT current_setting('server_version')").Scan(&psqlVer)
		log.Printf("successful connect to the PostgresSQL %s \n", psqlVer)
	}
	defer pool.Close()

	userRepository := factory.NewUserRepository(ctx, pool)
	userAuthService := factory2.NewUserAuthService(userRepository, cfg)
	isUpdated, err := userAuthService.UpdateDefaultAdminTokenByCfg(cfg.Auth)
	if err != nil {
		log.Fatalf("Can't update default Admin token: %v", err)
	} else if isUpdated {
		log.Println("Successfully updated default Admin token!")
	}

	authTokenSettingRep := factory.NewAuthTokenSettingRepository(pool)
	settingRepository := factory.NewSettingRepository(pool, authTokenSettingRep)
	resourceRepository := factory.NewResourceRepository(ctx, pool)
	messageRepository := factory.NewMessageRepository(ctx, pool)
	journalRepository := factory.NewJournalRepository(ctx, pool)
	routeParameters :=
		server.RouteParameters{
			Config:             cfg,
			UserRepository:     userRepository,
			SettingRepository:  settingRepository,
			ResourceRepository: resourceRepository,
			MessageRepository:  messageRepository,
			JournalRepository:  journalRepository,
		}

	resourceService := services.NewResourceService(resourceRepository)
	authTokenService := services.NewAuthTokenService(cfg, authTokenSettingRep, resourceService, journalRepository)
	senderService := services.NewMessageSenderService(cfg, messageRepository, journalRepository, authTokenService)
	go senderService.Run(cfg.Sender)
	log.Println("Notifier server was run!")

	var mp = memoryParameters{
		Messages:  make(map[string]models.Message, cfg.InstantlySender.BufferSize),
		Resources: make(map[string]models.Resource, cfg.InstantlySender.BufferSize),
	}
	instMessageRepository := factory.NewInstantlyMessageRepository(mp.Messages, cfg.InstantlySender.BufferSize)
	instResourceRepository := factory.NewInstantlyResourceRepository(mp.Resources, cfg.InstantlySender.BufferSize)
	instRouteParameters :=
		server.RouteParameters{
			Config:             cfg,
			UserRepository:     userRepository,
			SettingRepository:  settingRepository,
			ResourceRepository: instResourceRepository,
			MessageRepository:  instMessageRepository,
			JournalRepository:  journalRepository,
		}
	instSenderService := services.NewMessageSenderService(cfg, instMessageRepository, journalRepository, authTokenService)
	go instSenderService.Run(cfg.InstantlySender.Sender)
	log.Println("Instantly notifier server was run!")

	var messParams = services.MessageRouteParameters{
		Config:             cfg,
		UserRepository:     userRepository,
		ResourceRepository: resourceRepository,
		MessageRepository:  messageRepository,
		SettingRepository:  settingRepository,
	}

	if cfg.Queue.IsRun {
		messService := services.NewMessageService(messParams)
		qService := services.NewQueueService(cfg.Queue, messService, userAuthService, journalRepository)
		var conn *amqp.Connection
		var errQ error
		conn, queueCh, errQ = qService.Connection()
		if errQ != nil {
			utils.LogFatalError("i can't connect to an RabbitMQ queue: %v", errQ)
		} else {
			log.Printf("Successfully connected to the RabbitMQ queue!")
		}
		closeConnCH := make(chan *amqp.Error)
		conn.NotifyClose(closeConnCH)
		go func() {
			err = <-closeConnCH
			utils.LogError("reconnect to the RabbitMQ: %v", err)
			conn, queueCh, errQ = qService.Connection()
			if errQ != nil {
				utils.LogFatalError("i can't reconnect to an queue: %v", errQ)
			}
			errQ = qService.CreateQueue(queueCh)
			if errQ != nil {
				utils.LogFatalError("i can't recreate to an queue: %v", errQ)
			}
		}()

		defer func() {
			errQ = qService.Disconnect(queueCh)
			if errQ != nil {
				utils.LogFatalError("i can't disconnect from an queue: %v", errQ)
			}
		}()

		errQ = qService.CreateQueue(queueCh)
		if errQ != nil {
			utils.LogFatalError("i can't create to an queue: %v", errQ)
		} else {
			log.Printf("I successfully created the RabbitMQ queue!")
		}
		err = qService.ConsumerRun(ctx, queueCh)
		if errQ != nil {
			utils.LogFatalError("i can't consumer run of an queue: %v", errQ)
		} else {
			log.Printf("I successfully run consumer of the RabbitMQ queue!")
		}
	}

	if cfg.GrayLog.IsRun {
		gelfWriter, errW := gelf.NewWriter(cfg.GrayLog.URL)
		if errW != nil {
			utils.LogFatalError("gelf.NewWriter: %s", errW)
		}
		// log to both stderr and graylog2
		log.SetOutput(io.MultiWriter(os.Stderr, gelfWriter))
		utils.LogInfo("GrayLog successfully run!")
	}

	handler := server.GetRouters(userAuthService, routeParameters, instRouteParameters)
	idleConnsClosed := make(chan struct{})
	sigint := make(chan os.Signal, 1)
	if cfg.ServerType == config.HTTPServer {
		var srv = http.Server{Addr: cfg.ServerAddress, Handler: handler}
		runHTTPServer(cfg, &srv)
		shutdownGracefullyHTTPServer(ctx, &srv, idleConnsClosed, sigint)
		<-idleConnsClosed
		fmt.Println("Server HTTP Shutdown gracefully")
		srv.Shutdown(ctx)
	} else if cfg.ServerType == config.GRPCServer {
		srv := grpc.NewServer()
		runGRPCServer(cfg, userAuthService, srv, routeParameters)
		shutdownGracefullyGRPCServer(srv, idleConnsClosed, sigint)
		<-idleConnsClosed
		fmt.Println("Server GRPC Shutdown gracefully")
		srv.GracefulStop()
	} else {
		utils.LogFatalError("Unknowned server type")
	}
}

func runHTTPServer(config config.Config, srv *http.Server) {
	log.Printf("Starting HTTP server on the port:%s", srv.Addr)
	if config.EnableHTTPS {
		c := services.NewServerCertificate509Service(1658, "Yandex.Praktikum", "RU")
		if err := c.SaveCertificateAndPrivateKeyToFiles("cert.pem", "private.key"); err != nil {
			utils.LogFatalError("I can't save certificate and private key to files: %v", err)
		}
		if err := srv.ListenAndServeTLS("cert.pem", "private.key"); err != http.ErrServerClosed {
			utils.LogFatalError("HTTP server ListenAndServe: %v", err)
		}
	} else {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			utils.LogFatalError("HTTP server ListenAndServe: %v", err)
		}
	}
}

func runGRPCServer(cfg config.Config, uh *auth.UserAuthService, srv *grpc.Server, p server.RouteParameters) {
	listen, err := net.Listen("tcp", ":3200")
	if err != nil {
		utils.LogFatalError("can't run GRPC server on the tcp port 3200: %v", err)
	}

	var s grpc3.UserServer
	s.Config = p.Config
	s.UserRepository = p.UserRepository
	upb.RegisterUserServer(srv, &s)

	ms := grpc3.MessageServer{
		Config:             cfg,
		UserAuth:           *uh,
		UserRepository:     p.UserRepository,
		ResourceRepository: p.ResourceRepository,
		MessageRepository:  p.MessageRepository,
		JournalRepository:  p.JournalRepository,
	}
	mpb.RegisterMessageServer(srv, &ms)

	fmt.Println("Сервер gRPC начал работу")
	if err = srv.Serve(listen); err != nil {
		utils.LogFatalError("can't run GRPC server: %v", err)
	}
}

func shutdownGracefullyHTTPServer(ctx context.Context, srv *http.Server, idleConnsClosed chan struct{}, sigint chan os.Signal) {
	signal.Notify(sigint, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)
	go func() {
		<-sigint
		// получили сигнал os.Interrupt, запускаем процедуру graceful shutdown
		if err := srv.Shutdown(ctx); err != nil {
			// ошибки закрытия Listener
			utils.LogError("HTTP server Shutdown: %v", err)
		}
		// сообщаем основному потоку, что все сетевые соединения обработаны и закрыты
		close(idleConnsClosed)
	}()
}

func shutdownGracefullyGRPCServer(srv *grpc.Server, idleConnsClosed chan struct{}, sigint chan os.Signal) {
	signal.Notify(sigint, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)
	go func() {
		<-sigint
		// получили сигнал os.Interrupt, запускаем процедуру graceful shutdown
		srv.GracefulStop()
		// сообщаем основному потоку, что все сетевые соединения обработаны и закрыты
		close(idleConnsClosed)
	}()
}
