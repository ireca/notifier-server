package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/caarlos0/env/v6"
	"github.com/jinzhu/copier"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/ireca/notifier-server/internal/utils"
	"os"
)

type Config struct {
	ServerAddress     string          `env:"SERVER_ADDRESS" json:"serverAddress"`          // Server address.
	DatabaseDsn       string          `env:"DATABASE_DSN" json:"databaseDsn"`              // Connection string to the database.
	RequestTimeoutSec int64           `env:"REQUEST_TIMEOUT_SEC" json:"requestTimeoutSec"` // Timeout value for a request handling of the HTTP server.
	EnableHTTPS       bool            `env:"ENABLE_HTTPS" json:"enableHttps,omitempty"`    // Enable HTTPS connection.
	ConfigFilePath    string          `env:"CONFIG" json:"configFilePath,omitempty"`       // Filename of the server configurations.
	ServerType        string          `env:"SERVER_TYPE" json:"serverType,omitempty"`      // Type of the server: GRPS, HTTP
	Auth              Auth            `json:"auth"`
	Sender            Sender          `json:"sender"`
	InstantlySender   InstantlySender `json:"instantlySender"`
	Resource          Resource        `json:"resource"`
	Queue             Queue           `json:"queue"`
	GrayLog           Graylog         `json:"grayLog"`
	Sentry            Sentry          `json:"sentry"`
}

type Auth struct {
	AdminAuthToken string `env:"ADMIN_AUTH_TOKEN"` // Default Administrator token.
}

type Journal struct {
	IsWriteSentRequest    bool `json:"isWriteSentRequest"`
	IsWriteNotSentRequest bool `json:"isWriteNotSentRequest"`
}

type Sender struct {
	ItemCount               int     `env:"SENDER_MESSAGES_MAX" json:"itemCount"`     // Max count request.
	MaxAttempts             int     `env:"SENDER_ATTEMPT_MAX" json:"maxAttempts"`    // Max count attempts sends request to a resource.
	Timeout                 int     `env:"SENDER_REQUEST_TIMEOUT" json:"timeout"`    // Timeout to get response from a resource.
	LoadInterval            int     `env:"SENDER_LOAD_INTERVAL" json:"loadInterval"` // Server will be load messages according to the specified interval.
	IsDeleteNotSentMessages bool    `json:"isDeleteNotSentMessages"`
	IsDeleteSentMessages    bool    `json:"isDeleteSentMessages"`
	Journal                 Journal `json:"journal"`
}

type InstantlySender struct {
	BufferSize int     `env:"BUFFER_SIZE" json:"bufferSize"` // Max memory buffer size.
	Sender     Sender  `json:"sender"`
	Journal    Journal `json:"journal"`
}

type Resource struct {
	FireBase FireBase `json:"firebase"`
}

type FireBase struct {
	URLs         []string      `json:"urls,omitempty"`
	Applications []Application `json:"applications"`
}

type Application struct {
	ProjectId   string `json:"projectId"`
	CrlFilePath string `json:"crlFilePath"`
}

type Queue struct {
	IsRun      bool          `env:"QUEUE_IS_RUN" json:"isRun"`
	URL        string        `env:"QUEUE_URL" json:"url"`
	Name       string        `env:"QUEUE_NAME" json:"name"`
	Durable    bool          `env:"QUEUE_DURABLE" json:"durable"`
	AutoDelete bool          `env:"QUEUE_AUTO_DELETE" json:"autoDelete"`
	Exclusive  bool          `env:"QUEUE_EXCLUSIVE" json:"exclusive"`
	NoWait     bool          `env:"QUEUE_NO_WAIT" json:"noWait"`
	Args       amqp091.Table `json:"args"`
	Consumer   QueueConsumer `json:"consumer"`
	Exchange   QueueExchange `json:"exchange"`
	Bind       QueueBind     `json:"bind"`
}

type QueueConsumer struct {
	Name      string        `env:"QUEUE_CONSUMER_NAME" json:"name"`
	Consumer  string        `env:"QUEUE_CONSUMER" json:"consumer"`
	AutoAck   bool          `env:"QUEUE_AUTO_ASK" json:"AutoAck"`
	Exclusive bool          `env:"QUEUE_EXCLUSIVE" json:"exclusive"`
	NoLocal   bool          `env:"QUEUE_NO_LOCAL" json:"noLocal"`
	NoWait    bool          `env:"QUEUE_NO_WAIT" json:"noWait"`
	Args      amqp091.Table `json:"args"`
}

type QueueExchange struct {
	Name       string        `env:"QUEUE_EXCHANGE_NAME" json:"name"`
	Kind       string        `env:"QUEUE_EXCHANGE_KIND" json:"kind"`
	Durable    bool          `env:"QUEUE_EXCHANGE_DURABLE" json:"durable"`
	AutoDelete bool          `env:"QUEUE_EXCHANGE_AUTO_DELETE" json:"autoDelete"`
	Internal   bool          `env:"QUEUE_EXCHANGE_INTERNAL" json:"internal"`
	NoWait     bool          `env:"QUEUE_EXCHANGE_NO_WAIT" json:"noWait"`
	Args       amqp091.Table `json:"args"`
}

type QueueBind struct {
	Key    string        `env:"QUEUE_BIND_KEY" json:"key"`
	NoWait bool          `env:"QUEUE_BIND_NO_WAIT" json:"noWait"`
	Args   amqp091.Table `json:"args"`
}

type Graylog struct {
	IsRun bool   `env:"GRAY_LOG_IS_RUN" json:"isRun"`
	URL   string `env:"GRAY_LOG_URL" json:"url"`
}

type Sentry struct {
	IsRun              bool     `env:"SENTRY_IS_RUN" json:"isRun"`
	Dsn                string   `env:"SENTRY_DSN" json:"dsn"`
	Debug              bool     `env:"SENTRY_DEBUG" json:"debug"`
	AttachStacktrace   bool     `env:"SENTRY_ATTACH_STACKTRACE" json:"attachStacktrace"`
	SampleRate         float64  `env:"SENTRY_SAMPLE_RATE" json:"sampleRate"`
	EnableTracing      bool     `env:"SENTRY_ENABLE_TRACING" json:"enableTracing"`
	TracesSampleRate   float64  `env:"SENTRY_TRACES_SAMPLE_RATE" json:"tracesSampleRate"`
	IgnoreErrors       []string `json:"ignoreErrors"`
	IgnoreTransactions []string `json:"ignoreTransactions"`
	SendDefaultPII     bool     `env:"SENTRY_SEND_DEFAULT_PII" json:"sendDefaultPII"`
	ServerName         string   `env:"SENTRY_SERVER_NAME" json:"serverName"`
	Release            string   `env:"SENTRY_RELEASE" json:"release"`
	Dist               string   `env:"SENTRY_DIST" json:"dist"`
	Environment        string   `env:"SENTRY_ENVIRONMENT" json:"environment"`
	MaxBreadcrumbs     int      `env:"SENTRY_MAX_BREADCRUMBS" json:"maxBreadcrumbs"`
	MaxSpans           int      `env:"SENTRY_MAX_SPANS" json:"maxSpans"`
	MaxErrorDepth      int      `env:"SENTRY_MAX_ERROR_DEPTH" json:"maxErrorDepth"`
}

var cfg Config

const (
	HTTPServer = "http"
	GRPCServer = "grpc"
)

// GetConfigSettings gets configuration settings of the server.
func GetConfigSettings(configFromFile *Config) (Config, error) {
	const (
		RequestTimeoutSec  = 60
		SenderItemCount    = 10
		SenderMaxAttempts  = 3
		SenderTimeout      = 3
		SenderLoadInterval = 1
	)

	if configFromFile != nil {
		err := copier.Copy(&cfg, &configFromFile)
		if err != nil {
			return cfg, fmt.Errorf("I can't copy json config: %w", err)
		}
	}

	err := env.Parse(&cfg)
	if err != nil {
		return cfg, fmt.Errorf("I can't parse config: %w", err)
	}

	flag.StringVar(&cfg.ServerAddress, "a", cfg.ServerAddress, "The address of the local server")
	flag.StringVar(&cfg.DatabaseDsn, "d", cfg.DatabaseDsn, "Database port")
	flag.Int64Var(&cfg.RequestTimeoutSec, "rt", cfg.RequestTimeoutSec, "Timeout value for a request")
	flag.BoolVar(&cfg.EnableHTTPS, "e", cfg.EnableHTTPS, "Enable HTTPS")
	flag.StringVar(&cfg.ConfigFilePath, "c", cfg.ConfigFilePath, "Name of the config file")
	flag.StringVar(&cfg.ServerType, "s", cfg.ServerType, "Server type")
	flag.Parse()

	if cfg.ServerAddress == "" {
		return cfg, fmt.Errorf("unknowned server adress")
	}
	if cfg.DatabaseDsn == "" {
		return cfg, fmt.Errorf("unknowned database connection string")
	}
	if cfg.Auth.AdminAuthToken == "" {
		return cfg, fmt.Errorf("unknowned admin auth token")
	}

	if cfg.RequestTimeoutSec == 0 {
		cfg.RequestTimeoutSec = RequestTimeoutSec
	}
	if cfg.Sender.ItemCount == 0 {
		cfg.Sender.ItemCount = SenderItemCount
	}
	if cfg.Sender.MaxAttempts == 0 {
		cfg.Sender.MaxAttempts = SenderMaxAttempts
	}

	if cfg.Sender.Timeout == 0 {
		cfg.Sender.Timeout = SenderTimeout
	}

	if cfg.Sender.LoadInterval == 0 {
		cfg.Sender.LoadInterval = SenderLoadInterval
	}

	cfg.InstantlySender.Sender.LoadInterval = SenderLoadInterval
	cfg.InstantlySender.Sender.IsDeleteSentMessages = true
	cfg.InstantlySender.Sender.IsDeleteNotSentMessages = true
	cfg.InstantlySender.Journal.IsWriteNotSentRequest = false
	cfg.InstantlySender.Journal.IsWriteSentRequest = false

	return cfg, nil
}

// LoadConfigFile this method read a server configurations from a file in the json format.
func LoadConfigFile(configFilePath string) (*Config, error) {
	var configFromFile Config

	file, err := os.OpenFile(configFilePath, os.O_RDONLY, 0777)
	if err != nil {
		return nil, fmt.Errorf("unable to open a configuration file: %w", err)
	}
	defer utils.ResourceClose(file)

	info, err := file.Stat()
	if err != nil {
		return nil, fmt.Errorf("unable to get statistic info about configuration file: %w", err)
	}
	filesize := info.Size()
	jsonConfig := make([]byte, filesize)

	_, err = file.Read(jsonConfig)
	if err != nil {
		return nil, fmt.Errorf("i can't read a configuration file: %w", err)
	}

	err = json.Unmarshal(jsonConfig, &configFromFile)
	if err != nil {
		return nil, fmt.Errorf("i can't parse a configuration json file: %w", err)
	}

	return &configFromFile, nil
}
