# Start

Server
1. Clone repository from the Github.
2. Execute sql dump "/migrations/000001_create_tables.up.sql" or run command:"migrate -database postgres://postgres:password@localhost:5433/postgres?sslmode=disable -path migrations up"
3. Run tests in the "server" directory;
4. The project has enough ready http commands for test in the "http" folder.

P.S. Technical requirement link: https://docs.google.com/document/d/1XPMpVnMi3nqlURClIMtE1I1_ftb0WNpbilFuar4_1MU/edit#heading=h.jvhcof51f76j


