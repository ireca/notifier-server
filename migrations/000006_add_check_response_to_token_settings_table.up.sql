alter table public.ns_token_settings
    ADD column check_token_sub_response character varying(300) DEFAULT ''::character varying NOT NULL;