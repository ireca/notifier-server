CREATE TABLE public.ns_token_settings (
    id integer NOT NULL,
    setting_id integer NOT NULL,
    token character varying(1200) DEFAULT ''::character varying NOT NULL,
    url character varying(1200) DEFAULT ''::character varying NOT NULL,
    headers character varying(150)[] NOT NULL,
    body character varying(300) DEFAULT ''::character varying NOT NULL,
    check_token_http_codes integer[] DEFAULT ARRAY[]::integer[] NOT NULL,
    description character varying(100) DEFAULT ''::character varying NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);
ALTER TABLE public.ns_token_settings OWNER TO postgres;

CREATE SEQUENCE public.ns_token_settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.ns_token_settings_id_seq OWNER TO postgres;
ALTER SEQUENCE public.ns_token_settings_id_seq OWNED BY public.ns_token_settings.id;
ALTER TABLE ONLY public.ns_token_settings ALTER COLUMN id SET DEFAULT nextval('public.ns_token_settings_id_seq'::regclass);

ALTER TABLE ONLY public.ns_token_settings
    ADD CONSTRAINT ns_token_settings_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.ns_token_settings
    ADD CONSTRAINT ns_token_settings_pk2 UNIQUE (setting_id);

ALTER TABLE ONLY public.ns_token_settings
    ADD CONSTRAINT ns_messages_ns_token_settings_id_fk FOREIGN KEY (setting_id) REFERENCES public.ns_settings(id) ON UPDATE CASCADE ON DELETE CASCADE;
