SELECT 'CREATE DATABASE notifier_server'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'notifier_server')\gexec

ALTER DATABASE notifier_server OWNER TO postgres;