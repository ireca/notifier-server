CREATE TYPE public.resource_auth AS ENUM (
    'oauth2',
    'sdk',
    'none'
);
ALTER TYPE public.resource_auth OWNER TO postgres;

alter table public.ns_settings
    ADD column success_http_statuses integer[],
    ADD column auth_type public.resource_auth;